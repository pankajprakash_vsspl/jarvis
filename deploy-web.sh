#!bash
##
# @author       Pankaj Prakash
# @version      0.1
# @lastmodified 2 Aug 2018 14:52
# 
# @description
# The following script will deploy Kalgudi webapp UI to production.
# 
# Workflow of the webapp production deployment.
#  1. Navigate to website local repository.
#  2. Stash current git changes.
#  3. Pull latest code from remote website repository.
#  4. Move to webapp-deployment directory.
#  5. Delete everything except for node_modules and package.json file.
#  6. Copy everything from `webapp/app` directory to `webapp-deployment` 
#     directory.
#  7. Navigate to `webapp-deployment` directory and run grunt there.
#  8. Copy `webapp-deployment/build` and `fingerprint.json` file to local 
#     `webapp` repository.
# --------------------------------------------------------------------------------------------
#  9. Extract latest fingerprint from `fingerprint.json` file to some 
#     variable.
# 10. Backup `kalgudi.properties.js` and `web_index.html` file for raising
#     reverting PR.
# 11. Copy the fingerprint to `kalgudi.properties.js` file after prefixing
#     with `?v=fingerprint`.
# 12. Modify `web_index.html` with following changes.
#     1. Replace style.css line with new style.css?v=fingerprint line.
#     2. Find and remove all lines that are grunt.
#     3. Replace the web.min script inclusion line with web.min?v=fingerpint.
# 13. Commit and push all changes with proper webapp production deployment message.
# 14. Open jenkins deployment tab and bitbucket tab to raise deployment PR.
# 
# All the above process should be jump to next step only if the predecessor gets 
# completed successfully. 
##


##
# -------------------------- Variables --------------------------
##

# Error code must be set with on each command run with their 
# run status
errCode=0

# Required directories path for webapp deployment
curDir=$(pwd)

webDir=~/Documents/vsspl/website/
webBuildPath=$webDir'app/build'
webFingerprintPath=$webDir'app/fingerprint.json'

webDeploymentDir=~/Documents/vsspl/website-deployment/
webDeploymentBuildPath=$webDeploymentDir'build'
webDeploymentFingerprintPath=$webDeploymentDir'fingerprint.json'

webBackupDir=~/Documents/vsspl/website-backup/



# Kalgudi properties file path
kalgudiPropFile=~/Documents/vsspl/website/app/common/primary/kalgudi.properties.js

# Kalgudi latest fingerprint file path
fingerPrintFile=~/Documents/vsspl/website/app/fingerprint.json

# Kalgudi web index file path
webIndexFile=~/Documents/vsspl/website/app/web_index.html

# Backup files path
kalgudiPropBackupFile=$webBackupDir'kalgudi.properties.js'
webIndexBackupFile=$webBackupDir'web_index.html'

# Stores latest fingerprint
fingerprint=''




##
# -------------------------- Procedures --------------------------
# 



main() {

    # Deployment message
    printf '\e[30;32mBoss on work, I will take care of Kalgudi webapp UI deployment.\e[0;97;49m'


    # Navigate to webapp local repo directory
    cd $webDir


    if [ $? -ne 0 ]; then
        # Webapp local repository directory navigation failed
        logError 'Unable to navigate to webapp local repository.'
        pushNotification 'Build Failed: Cannot find local webapp repository.'

        exit 1
    fi

    # Take latest code from remote repository
    getLatestCode

    # Copy latest code to deployment directory
    copyCodeToDeploymentDir

    # Minify latest code
    minifyCode

    # Copy minified code to local repository
    copyMinifiedCodeToLocalRepo

    # Get latest fingerprint generated using grunt process
    getLatestFingerprint
}


##
# Takes, latest code from remote webapp repository. 
# Stashes current changes in the local webapp repository directory.
# 
# The method itself handles all error paths related to fetching latest
# code from repository.
#
# @returns 0 on success otherwise exits from app.  
# 
getLatestCode() {

    log 'Fetching latest code from webapp remote repository.'

    # Stash current changes in webapp directory
    git stash


    $errCode=$?

    # Stashed local webapp repository
    if [ $? -e 0 ]; then

        # Get latest code from dev branch
        git pull --rebase upstream dev
    else

        # Could not stash current changes
        logError 'Unable to stash current changes.'
        pushNotification 'Build Failed: Tried stashing changes, but failed to do.'


        # Noting to do much with this function hence return error code
        # return "$errCode"
        exit "$errCode"
    fi

    

    $errCode=$?

    # Unable to fetch latest code
    if [ $errCode -ne 0 ]; then

        logError 'Unable to fetch latest code from bitbucket.'

        # Test internet connection
        testBitbucketConnectivity

        if [ $? -e 0 ]; then
            pushNotification 'Build Failed: Could not fetch latest code from remote.'
        else
            pushNotification 'Build Failed: No internet, could not fetch latest code from remote.'
        fi

        exit "$errCode"
        
    fi


    return "$errCode"
}


##
# Copies lastest code to webapp deployment directory. 
# 
# @returns 0 on success otherwise exits from app.
# 
copyCodeToDeploymentDir() {

    log 'Taking latest code to web deployment directory.'

    # Delete build directory, since it will be generated during the deployment process
    rm -rf $webBuildDir

    # Recursively copy all files and directories to app
    cp -R -f $webDir'/app/*' $webDeploymentDir

    $errCode=$?

    # Check for any errors while copying latest code
    if [ $errCode -ne 0 ]; then
        logError 'Unable to copy latest code to web deployment directory.'
        pushNotification 'Build Failed: Could not copy latest code to web deployment directory.'

        exit "$errCode"
    fi

    return "$errCode"
}


##
# Minifies latest Kalgudi code using `grunt` tool. Performing this operation
# will generate all minified files in `build` directory. It also generates a 
# unique signature in `fingerprint.json` file.
# 
# @return 0 on success otherwise exits from app
# 
minifyCode() {

    log 'Hold on, minifying your scripts. This may take a while...'

    # Navigate to webapp-deployment directory
    cd $webDeploymentDir

    # Run grunt to minify files
    grunt

    $errCode=$?

    # Handle errors if any
    if [ $errCode -ne 0 ]; then
        logError 'Minification failed, something wrong with your script. Please fix minification errors and try again.'
        pushNotification 'Build Failed: Code minification failed. Please check build logs.'

        exit "$errCode"
    fi

    return "$errCode"

}


##
# Copy latest build directory and fingerprint file to local repository.
# 
# @returns 0 on success otherwise exits from app.
# 
copyMinifiedCodeToLocalRepo() {

    # Copy build directory to web repository
    cp -R -f $webDeploymentBuildPath $webBuildPath

    $errCode=$?

    # Check for errors while copying build directory 
    if [ $errCode -ne 0 ]; then
        logError 'Unable to copy minified build scripts to local repository.'
        pushNotification 'Build Failed: Unable to copy minified code to local repository.'

        exit 1
    fi

    # Copy fingerprint.json to web repository
    cp -R -f $webDeploymentFingerprintPath $webFingerprintPath


    $errCode=$?

    # Check for errors while copying fingerprint.json file
    if [ $errCode -ne 0 ]; then
        logError 'Unable to copy latest fingerprint to local repository.'
        pushNotification 'Build Failed: Unable to latest fingerprint to local repository.'

        exit 1
    fi

    return "$errCode"
}


# 
# Gets, latest fingerprint that is generated during the grunt process.
# 
getLatestFingerprint() {

    $fingerprint=grep -o '[a-zA-Z0-9]*' $webFingerprintPath

    # Check for valid fingerprint
    if [ ! "$fingerprint" ]; then
        logError 'Unable to fetch latest fingerprint.'
        pushNotification 'Build Failed: Cannot fetch fingerprint'
        exit 1
    else
        log "Latest fingerprint $fingerprint"
    fi
}


# 
# Updates web_index.html file for production deployment.
# Removes all unnecessary script tags i.e. minified scripts imports.
# Updates style and webindex with latest fingerprint.
# 
updateWebIndex() {
    
}




##
# Send notification to ubuntu system. By defualt it sends show notification for
# 10 seconds. 
# It accepts a parameter i.e. the message to show within notification.
# 
pushNotification() {
    notify-send -t 10000 -i ~/Documents/scripts/jarvis/assets/ai.png 'Jarvis - Webapp' "$1"
}


##
# Prints error message on console.
# It accepts a parameter i.e. the message to show.
# 
logError() {
    printf "\n\e[1;37;41m[WARN]\e[0;97;49m : $1\n\n"
}


##
# Prints a message on console.
# It accepts a parameter i.e. the message to show.
# 
log() {
    printf "\n\n\e[1;37;42m[INFO]\e[0;97;49m : $1\n"
}


##
# Tests internet connectivity. The function will call system
# ping command for 4 times to check internet connectivity issue 
# with bitbucket.
# 
# Returns 0 if ping was successful.
# 
testBitbucketConnectivity() {

    log 'Testing internet connectivity. Please wait...'

    ping -c 4 https://bitbucket.org

    return "$?"
}




# -------------------------- Nothing beyond -------------------------- #

# Black background with red foreground
printf '\e[30;31m'

cat << "EOF"


888               888                        888 d8b 
888               888                        888 Y8P 
888               888                        888     
888  888  8888b.  888  .d88b.  888  888  .d88888 888 
888 .88P     "88b 888 d88P"88b 888  888 d88" 888 888 
888888K  .d888888 888 888  888 888  888 888  888 888 
888 "88b 888  888 888 Y88b 888 Y88b 888 Y88b 888 888 
888  888 "Y888888 888  "Y88888  "Y88888  "Y88888 888 .v0
                           888                      
                      Y8b d88P       - UI deployment
                       "Y88P"     (c) Pankaj Prakash
                                                        
EOF

# Normal background and foreground
printf '\e[0;97;49m'


# Handover the control to the main driver function
main