##
# @author Pankaj Prakash
# @lastmodified 2ng Aug 2018, 14:35
# @version 3
# 
# @description
# The following script will handle dashboard deployment. It provides a hassel
# free dashboard deployment. It takes care of any error during deployment and 
# updates the user with proper notification messages about the deployment.
# 
# Workflow of dashboard deployment: 
# 1. Navigate to dashboard deployment directory.
# 2. Stash and take latest dashboard code from dashboard remote repository.
# 3. Build dashboard project.
# 4. Push dashbaord build directory to S3.
# 
# In case of above workflow the script handles all errors and ensure not to
# push any invalid files to dashboard S3 bucket..
##


##
# -------------------------- Variables --------------------------
##
deploymentDir=~/Documents/vsspl/dashboard-deployment/dashboards/


# Clear screen before doing anything
clear





##
# -------------------------- Procedures --------------------------
##


##
# Main driver function to handle dashboard deployment.
# 
deploy() {

    # Deployment message
    printf '\e[30;32mBoss on work, I will take care of dashboard deployment.\e[0;97;49m'


    # Move to dashboard deployment local directory
    cd $deploymentDir

    
    log 'Please wait while I pull latest code from Vasudhaika Dashboards.'


    # Remove current changes in local git repo
    git checkout dev
    git stash
    

    # Pull latest changes from remote
    if [ $? -eq 0 ]; then
        log 'Pulling latest code from remote Dashboard repository.'
        git pull --rebase upstream dev
    else
        logError 'Build failed, unable to stash local Dashboard repo.'
        pushNotification 'Build Failed: Something wrong with dashboard deployment repository.'
        exit 1
    fi


    # Build dashboard
    if [ $? -eq 0 ]; then

        # Run npm
        # npm install

        log 'Building dashboard project for deployment.'
        # ng build --prod --build-optimizer
        npm run build-prod

    else
        logError 'Unable to pull lastest code from remote repository.'

        # Check if unable to pull latest code due to internet connectivity
        testBitbucketConnectivity

        if [ $? -eq 0 ]; then
            pushNotification 'Build Failed: Cannot pull latest code from remote repository.'
        else
            pushNotification 'Build Failed: No internet connectivity.'
        fi

        exit 1
    fi

    # Build dashboard
    if [ $? -eq 0 ]; then
        cd dist
    else
        logError 'Build Failed, please check build logs.'
        pushNotification 'Build Failed: Unable to build dashboard project.'
        exit 1
    fi


    # Deploy dashboard
    if [ $? -eq 0 ]; then
        log 'Deploying to S3.'

        # Push everything in dist folder to S3 with public read permission
        # The command also deletes unwanted files.
        aws s3 sync . s3://dashboard.kalgudi.com/ --acl public-read --delete
        cd ..
    else
        logError 'Build Failed, please check build logs.'
        pushNotification 'Build Failed: Unable to build dashboard project.'
        exit 1
    fi


    # S3 Sync success
    if [ $? -eq 0 ]; then
        log 'Dashboard deployed successfully => https://dashboard.kalgudi.com'

        pushNotification 'Build Success: Hello sir, dashboard pushed to production.'

        # Open dashboard to test
        # google-chrome --incognito "https://dashboard.kalgudi.com"

    else
        logError 'S3 upload failed.'
        pushNotification 'Build Failed: I am unable to upload files to S3.'
        exit 1
    fi

}



##
# Send notification to ubuntu system. By defualt it sends show notification for
# 10 seconds. 
# It accepts a parameter i.e. the message to show within notification.
# 
pushNotification() {
    notify-send -t 1800000 -i ~/Documents/scripts/jarvis/assets/ai.png 'Jarvis - Dashboard' "$1"
}


##
# Prints error message on console.
# It accepts a parameter i.e. the message to show.
# 
logError() {
    printf "\n\e[1;37;41m[WARN]\e[0;97;49m : $1\n\n"
}


##
# Prints a message on console.
# It accepts a parameter i.e. the message to show.
# 
log() {
    printf "\n\n\e[1;37;42m[INFO]\e[0;97;49m : $1\n"
}


##
# Tests internet connectivity. The function will call system
# ping command for 4 times to check internet connectivity issue 
# with bitbucket.
# 
# Returns 0 if ping was successful.
# 
testBitbucketConnectivity() {

    log 'Testing internet connectivity. Please wait...'

    ping -c 4 https://bitbucket.org

    return "$?"
}





# -------------------------- Nothing beyond -------------------------- #

# Black background with red foreground
printf '\e[30;31m'
cat << "EOF"


     888                   888      888                                     888 
     888                   888      888                                     888 
     888                   888      888                                     888 
 .d88888  8888b.  .d8888b  88888b.  88888b.   .d88b.   8888b.  888d888  .d88888 
d88""888     "88b 88K      888 "88b 888 "88b d88""88b     "88b 888P"   d88" 888 
888  888 .d888888 "Y8888b. 888  888 888  888 888  888 .d888888 888     888  888 
Y88b 888 888  888      X88 888  888 888 d88P Y88..88P 888  888 888     Y88b 888 
 "Y88888 "Y888888  88888P' 888  888 88888P"   "Y88P"  "Y888888 888      "Y88888 .v3
                                                             (c) Pankaj Prakash
                                                        
EOF

# Normal background and foreground
printf '\e[0;97;49m'

deploy

# Happy coding ;)
