# How to login with production data in Kalgudi dev environment?  


```


888    d8P           888                        888 d8b  
888   d8P            888                        888 Y8P  
888  d8P             888                        888      
888d88K      8888b.  888  .d88b.  888  888  .d88888 888  
8888888b        "88b 888 d88P"88b 888  888 d88" 888 888  
888  Y88b   .d888888 888 888  888 888  888 888  888 888  
888   Y88b  888  888 888 Y88b 888 Y88b 888 Y88b 888 888  
888    Y88b "Y888888 888  "Y88888  "Y88888  "Y88888 888  
                              888 888                       
                         Y8b d88P 888                       
                          "Y88P"  888                       
                              .d88888  .d88b.  888  888 
                             d88" 888 d8P  Y8b 888  888 
                             888  888 88888888 Y88  88P 
                             Y88b 888 Y8b.      Y8bd8P  
                              "Y88888  "Y8888    Y88P   


```
----------

1. Make sure that you have opened google chrome in disabled web security mode. If not please follow steps below.

    * Kill all chrome processes. Open terminal and hit `killall google-chrome` and `killall chrome`.  
    * Once all process with chrome are killed, start google chrome with disabled web security. Hit following command `google-chrome --disable-web-security --user-data-dir`.  
      **NOTE:** Don't close the terminal that started google chrome.  

2. Copy entire website repository to your local dev directory. You can exclude `app\build` directory.

3. Modify `kalgudi.properties.js` accordingly to point to production endpoints with secure protocol i.e. `https`.

4. Modify first few lines of `loginNew.js` to log in with production credentials.  
  
        var SERVICES_URL,IP_DOMAIN;
        IP_DOMAIN = 'www.kalgudi.com';
        
        // Uncomment this, when devkalgudi is configured again
        if(window.location.hostname.indexOf("devkalgudi")> -1) {
            SERVICES_URL = "https://"+IP_DOMAIN;
        } else {
            SERVICES_URL = "https://"+IP_DOMAIN;
        }
  

5. Open [http://devkalgudi.vasudhaika.net/pankaj/login.html](http://devkalgudi.vasudhaika.net/pankaj/login.html). I have assumed that you have copied code to **_pankaj_** directory.

Yippee!!! you made it.