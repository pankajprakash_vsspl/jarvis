const data = [
    {
        "mainCategory": "Cereals & Grains",
        "subCategory1": " "
    },
    {
        "mainCategory": "Pulses",
        "subCategory1": " "
    },
    {
        "mainCategory": "Oils and Fats",
        "subCategory1": " "
    },
    {
        "mainCategory": "Fibre Crops",
        "subCategory1": " "
    },
    {
        "mainCategory": "Fruits & Vegetables",
        "subCategory1": " "
    },
    {
        "mainCategory": "Spices",
        "subCategory1": " "
    },
    {
        "mainCategory": "Live stock",
        "subCategory1": " "
    },
    {
        "mainCategory": "Poultry",
        "subCategory1": " "
    },
    {
        "mainCategory": "Fisheries",
        "subCategory1": " "
    },
    {
        "mainCategory": "Edible Nuts & Seeds",
        "subCategory1": " "
    },
    {
        "mainCategory": "Plantation Crops",
        "subCategory1": " "
    },
    {
        "mainCategory": "Flowers",
        "subCategory1": "Dried"
    },
    {
        "mainCategory": "Flowers",
        "subCategory1": "Fresh"
    },
    {
        "mainCategory": "Dyes, Gums & resins",
        "subCategory1": " "
    },
    {
        "mainCategory": "Forest Products",
        "subCategory1": " "
    },
    {
        "mainCategory": "Fodder & Grasses",
        "subCategory1": " "
    },
    {
        "mainCategory": "Dairy",
        "subCategory1": " "
    }
];

const hierarchy = {
    subCategories: {}
};

function addToCategory(_hierarchy, category) {

    if (category.id.length <= 0) {
        return _hierarchy;
    }

    _hierarchy.subCategories = _hierarchy.subCategories || {};
    _hierarchy.subCategories[category.id] = _hierarchy.subCategories[category.id] || {
        id: category.id,
        value: category.value
    };

    return _hierarchy;
}


function getNormalizedCategory(data) {
    data = (data || '').trim();
    
    let _id = (data || '').replace(/[,\.;\-\'\(\)]/g, '');
    _id = _id.replace(/[\&]/g, 'AND').replace(/\s/g, '_').toUpperCase()

    return {
        id: _id,
        value: data
    };
}

function constructHierarchy() {

    data.forEach(d => {

        const mainCategory = getNormalizedCategory(d.mainCategory);
        const subCategory1 = getNormalizedCategory(d.subCategory1);
        // const subCategory2 = getNormalizedCategory(d.subCategory2);
        // const subCategory3 = getNormalizedCategory(d.subCategory3);

        addToCategory(hierarchy, mainCategory);


        addToCategory(hierarchy.subCategories[mainCategory.id], subCategory1);
        // addToCategory(hierarchy.subCategories[mainCategory.id].subCategories[subCategory1.id], subCategory2);
        // addToCategory(hierarchy.subCategories[mainCategory.id].subCategories[subCategory1.id].subCategories[subCategory2.id], subCategory3);


        // console.log((hierarchy));
    });
}

/**
 * Processes categories in view consumable value
 */
function _processCategories(category) {

    // No subcategories return empty hand
    if (!category.subCategories) {
        return category;
    }

    // List of new categories
    var _categories = [];

    // Map all subcategories keys 
    const _subCategories = Object.keys(category.subCategories);

    for (var i = 0; i < _subCategories.length; i++) {

        const id = _subCategories[i];
        
        var _item = category.subCategories[id];

        _categories.push(_processCategories(_item));
    }

    delete category.subCategories;
    category.subCategories = _categories;
    
    return category;
}

constructHierarchy();
const searchCategory = _processCategories({subCategories: hierarchy.subCategories});

console.log('\n\n\n\n\n\n\n\n\n\n\n\n\n');

console.log(JSON.stringify(hierarchy));
console.log(JSON.stringify(searchCategory));