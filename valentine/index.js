/**
 * Valentine beings here
 */
"use strict";

const http = require("http");
const config = require("./config/config");
const handler = require('./src/handler');
const logger = require('./src/logger');

// Create a valentine
const server = http.createServer((req, res) => {

  // let method = req.method
  let body = '';
  // let path = purl.pathname;
  req.on('data',(data)=>{
      body += data;
  });

  req.on('end',()=>{
    let x = new Date();

    handler()
      .then((data) => {
        res.writeHead(data.status);
        res.end(data.body);
        let y = new Date();
        
        console.log(`[${new Date().toLocaleString()}] [INFO] IP:${req.connection.remoteAddress} \tMETHOD:${req.method} \tURL:${purl.path} \tresponseTime:${y-x}`);
      }).catch((error) => {

          console.error("[ERROR] [server.js] message ::",error.stack.split('\n').slice(0,2).toString());

          res.writeHead(404)
          res.end("ERROR");
      });
  });
});

// Execute your plan
function startServer() {

  // Valentine will now listen your orders
  server.listen(config.port, () => {
    console.log(process);
    logger.log('Valentine is UP @');
  });

  // Get informed when valentine is going down
  server.on('close', () => {
    logger.warn('Valentine is SHUTTING DOWN');
  });

  server.on('error', (err) => {
    logger.error('Valentine is MESSED UP');
  });

  server.on('connection', (socket) => {
    logger.log('Listening to your words');
  });
}

startServer();
