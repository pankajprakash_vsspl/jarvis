# This script raises webapp deployment PR
# It accepts two parameters 
# $1 branch name
# $2 Commit message

# Clear screen before doing anythin
clear

deploy() {

    # Navigate to website directory
    cd ~/Documents/vsspl/website/

    # Stage all changes
    git add .

    # Commit changes
    if [ $? -eq 0 ]; then
        printf '\n\e[1;37;42m[INFO]\e[0;97;49m : Commiting changes for deployment.\n\n'
        git commit -m "$2"
    else
        printf '\n\e[1;37;41m[WARN]\e[0;97;49m : Error while adding staging local repo files.\n\n'
        exit 1
    fi

    # Push changes to remote
    if [ $? -eq 0 ]; then
        printf '\n\e[1;37;42m[INFO]\e[0;97;49m : Pushing code to remote.\n\n'
        git push -f -u origin $1
    else
        printf '\n\e[1;37;41m[WARN]\e[0;97;49m : Error while commit changes.\n\n'
        exit 1
    fi


    # Raise new Pr
    if [ $? -eq 0 ]; then
        printf '\n\e[1;37;42m[INFO]\e[0;97;49m : All good, go and deploy production webapp from jenkins.\n\n'
        google-chrome "http://52.1.185.104:8080/login"
        google-chrome "https://bitbucket.org/pankajprakash_vsspl/website/pull-requests/new?source=dev&t=1"
    else
        printf '\n\e[1;37;41m[WARN]\e[0;97;49m : Error while pushing changes to remote.\n\n'
        exit 1
    fi


}

deploy $1 "$2"