ROOT=`pwd`

# Move to website
cd ~/Documents/vsspl/website/

WEBSITE_DIR=`pwd`

# Kalgudi properties relative file path from website
# PROPERTIES_FILE_PATH=$WEBSITE_DIR'/app/common/primary/kalgudi.properties.js'

# Delete remote files
sshpass -p 'vsspl' ssh -o StrictHostKeyChecking=no vsspl@192.168.1.5 'rm -rf /home/vsspl/Softwares/jetty/webapps/kalgudi-uss/'$1'/*'

# Take latest code in website 
git pull --rebase upstream dev

# Modify kalgudi properties for dev
# sed -i 's/"isProduction"\s*:\s*false/"isProduction": true/' $PROPERTIES_FILE_PATH
# sed -i 's/"enviorment"\s*:\s*ENV_PROD/"enviorment": ENV_DEV/' $PROPERTIES_FILE_PATH
# sed -i 's/"secure"\s*:\s*"https:\/\/"/"secure": "http:\/\/"/' $PROPERTIES_FILE_PATH

# Copy files from local to dev
sshpass -p 'vsspl' scp -r /home/pankaj/Documents/vsspl/website/app/* vsspl@192.168.1.5:/home/vsspl/Softwares/jetty/webapps/kalgudi-uss/$1/

# Delete build directory
sshpass -p 'vsspl' ssh -o StrictHostKeyChecking=no vsspl@192.168.1.5 'rm -rf /home/vsspl/Softwares/jetty/webapps/kalgudi-uss/'$1'/build'

# Stash the changes in local git website repo
# git stash


# Move to root dir
cd $ROOT