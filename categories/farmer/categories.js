const data = [
      {
          "mainCategory": "Farm Machinery",
          "subCategory1": "Aqua",
          "subCategory2": " "
      },
      {
          "mainCategory": "Farm Machinery",
          "subCategory1": "Farm implements",
          "subCategory2": " "
      },
      {
          "mainCategory": "Farm Machinery",
          "subCategory1": "Hand tools",
          "subCategory2": " "
      },
      {
          "mainCategory": "Farm Machinery",
          "subCategory1": "Mulching sheet",
          "subCategory2": " "
      },
      {
          "mainCategory": "Farm Machinery",
          "subCategory1": "Pumps",
          "subCategory2": " "
      },
      {
          "mainCategory": "Farm Machinery",
          "subCategory1": "Solar light",
          "subCategory2": " "
      },
      {
          "mainCategory": "Farm Machinery",
          "subCategory1": "Tarpaulins",
          "subCategory2": " "
      },
      {
          "mainCategory": "Farm Machinery",
          "subCategory1": "Tractors",
          "subCategory2": " "
      },
      {
          "mainCategory": "Farm Machinery",
          "subCategory1": "Traps & Lures",
          "subCategory2": " "
      },
      {
          "mainCategory": "Plant Nutrition",
          "subCategory1": "Adjuvants & Surfactants",
          "subCategory2": " "
      },
      {
          "mainCategory": "Plant Nutrition",
          "subCategory1": "Color enhancer",
          "subCategory2": " "
      },
      {
          "mainCategory": "Plant Nutrition",
          "subCategory1": "Fertilizers",
          "subCategory2": " "
      },
      {
          "mainCategory": "Plant Nutrition",
          "subCategory1": "Growth promoters",
          "subCategory2": " "
      },
      {
          "mainCategory": "Plant Nutrition",
          "subCategory1": "Micro nutrients ",
          "subCategory2": " "
      },
      {
          "mainCategory": "Plant Nutrition",
          "subCategory1": "Sugar enhancer",
          "subCategory2": " "
      },
      {
          "mainCategory": "Plant Protection",
          "subCategory1": "Bio/Organic",
          "subCategory2": "Bactericides"
      },
      {
          "mainCategory": "Plant Protection",
          "subCategory1": "Bio/Organic",
          "subCategory2": "Fungicides"
      },
      {
          "mainCategory": "Plant Protection",
          "subCategory1": "Bio/Organic",
          "subCategory2": "Insecticides"
      },
      {
          "mainCategory": "Plant Protection",
          "subCategory1": "Chemical",
          "subCategory2": "Bactericides"
      },
      {
          "mainCategory": "Plant Protection",
          "subCategory1": "Chemical",
          "subCategory2": "Fungicides"
      },
      {
          "mainCategory": "Plant Protection",
          "subCategory1": "Chemical",
          "subCategory2": "Herbicides"
      },
      {
          "mainCategory": "Plant Protection",
          "subCategory1": "Chemical",
          "subCategory2": "Insecticides"
      },
      {
          "mainCategory": "Plant Protection",
          "subCategory1": "Traps & Lures",
          "subCategory2": "Lures"
      },
      {
          "mainCategory": "Plant Protection",
          "subCategory1": "Traps & Lures",
          "subCategory2": "Traps"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Field crops",
          "subCategory2": "Corn"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Field crops",
          "subCategory2": "Cotton"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Field crops",
          "subCategory2": "Jowar"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Field crops",
          "subCategory2": "Maize"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Field crops",
          "subCategory2": "Mustard"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Field crops",
          "subCategory2": "paddy"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Horticulture",
          "subCategory2": "Flower seeds"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Horticulture",
          "subCategory2": "Fruits"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Horticulture",
          "subCategory2": "Vegetable seeds"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Saplings",
          "subCategory2": "Coconut"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Saplings",
          "subCategory2": "Papaya"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Special categories",
          "subCategory2": "Exotics"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Special categories",
          "subCategory2": "Forages"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Special categories",
          "subCategory2": "Home garden"
      },
      {
          "mainCategory": "Seeds",
          "subCategory1": "Special categories",
          "subCategory2": "Polyhouse"
      }
  ];

const hierarchy = {
    subCategories: {}
};

function addToCategory(_hierarchy, category) {

    if (category.id.length <= 0) {
        return _hierarchy;
    }

    _hierarchy.subCategories = _hierarchy.subCategories || {};
    _hierarchy.subCategories[category.id] = _hierarchy.subCategories[category.id] || {
        id: category.id,
        value: category.value
    };

    return _hierarchy;
}


function getNormalizedCategory(data) {
    data = (data || '').trim();
    
    let _id = (data || '').replace(/[,\.;\-\'\(\)]/g, '');
    _id = _id.replace(/[\&]/g, 'AND').replace(/\s/g, '_').toUpperCase()

    return {
        id: _id,
        value: data
    };
}

function constructHierarchy() {

    data.forEach(d => {

        const mainCategory = getNormalizedCategory(d.mainCategory);
        const subCategory1 = getNormalizedCategory(d.subCategory1);
        const subCategory2 = getNormalizedCategory(d.subCategory2);
        const subCategory3 = getNormalizedCategory(d.subCategory3);

        addToCategory(hierarchy, mainCategory);


        addToCategory(hierarchy.subCategories[mainCategory.id], subCategory1);
        addToCategory(hierarchy.subCategories[mainCategory.id].subCategories[subCategory1.id], subCategory2);
        // addToCategory(hierarchy.subCategories[mainCategory.id].subCategories[subCategory1.id].subCategories[subCategory2.id], subCategory3);


        // console.log((hierarchy));
    });
}

/**
 * Processes categories in view consumable value
 */
function _processCategories(category) {

    // No subcategories return empty hand
    if (!category.subCategories) {
        return category;
    }

    // List of new categories
    var _categories = [];

    // Map all subcategories keys 
    const _subCategories = Object.keys(category.subCategories);

    for (var i = 0; i < _subCategories.length; i++) {

        const id = _subCategories[i];
        
        var _item = category.subCategories[id];

        _categories.push(_processCategories(_item));
    }

    delete category.subCategories;
    category.subCategories = _categories;
    
    return category;
}

constructHierarchy();
const searchCategory = _processCategories({subCategories: hierarchy.subCategories});

console.log('\n\n\n\n\n\n\n\n\n\n\n\n\n');

console.log(JSON.stringify(hierarchy));
console.log(JSON.stringify(searchCategory));