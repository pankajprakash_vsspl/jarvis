/**
 * Shits done by valentine
 */

'use strict';

// Module container
var log = {};

/**
 * Prints an information message on console. It prints the information message
 * in green.
 * 
 * @param {string} msg Message to print on console.
 */
log.info = function (msg) {
  console.log(`\x1b[32m\x1b[1m[INFO] ${_getFormattedDate()}\x1b[0m ${msg}`)
};
log.log = (msg) => log.info(msg);

/**
 * Prints a warning message on console. It prints the warning message
 * in magenta.
 * 
 * @param {string} msg Message to print on console.
 */
log.warn = function (msg) {
  console.warn(`\x1b[35m\x1b[1m[WARN] ${_getFormattedDate()}\x1b[0m ${msg}`)
};

/**
 * Prints an error message on console. It prints the error message
 * in magenta.
 * 
 * @param {string} msg Message to print on console.
 */
log.error = function (msg) {
  console.error(`\x1b[31m\x1b[1m[ERROR] ${_getFormattedDate()}\x1b[0m ${msg}`)
};

function _getFormattedDate() {

  
  const monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];
  
  const date = new Date();

  const day = date.getDate();
  const monthIndex = date.getMonth();
  const year = date.getFullYear();
  const hour = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();
  const milliseconds = date.getMilliseconds();

  return `${hour}:${minutes}:${seconds} ${milliseconds}`; // day + ' ' + monthNames[monthIndex] + ' ' + year;
};


// Module exports
module.exports = log;
