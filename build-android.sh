#!/bin/bash
# 
# @author       Pankaj Prakash
# @description  This script automates the android build process for Kalgudi mobile app 
# for both development and production apk. 
#                 
# Android Build process:
#   1.  Create backup of previous successful android build.
#   2.  Get latest code from remote git to your local repo.
#   3.  Copy app directory from local repo to kalgudiLollypop directory
#       to kalgudiLollypop/www/app/ directory.
#   4.  Copy Gruntfile.js from local repo to kalgudiLollypop root
#       directory.
#   5.  Modify kalgudi.properties.js file for production or development 
#       in kalgudiLollypop app directory.
#   6.  Run grunt to minify Javascripts.
#   7.  Delete all webapp contents, since its not needed for mobile app.
#   8.  Delete images used in webapp.
#   9.  Delete common scripts not used in mobile app.
#   10. Goto kalgudiLollypop root directory and run 
#       'cordova build android' to build APK.
# 
# New features in v2
# 1. The script has got more power and handles all error itself.
# 2. It takes care of updating the version code and number automatically.
#                       
# 
# @lastmodified Aug 21, 2018
# 
#----------------------------------------------------------------------------------------- 




# 
# ----------------------------------- DEFAULT SETTINGS -----------------------------------
# 


# true if you want default build for production
# false if you want default build for dev
BUILD_FOR_PRODUCTION=false

# true if default build should be signed
# false if default build should not be signed
# NOTE: Signed version of APK is for playstore upload. Otherwise build unsigned.
BUILD_SIGNED_APK=false

# true if default build should be crosswalk
# false if default build should be non-crosswalk
# Crosswalk is a device compitability build to support older version devices
BUILD_WITH_CROSSWALK=false

# true to backup entire contents of current android building directory.
# false to skip backup
# NOTE: The backup is compressed gzip tar format.
IS_BACKUP_REQUIRED=false

# true to pull latest code from git
# false not to pull latest code from git and work with the existing code in local repository/
PULL_FROM_GIT=false

# true to show help menu by default.
# false not to show help menu by default
SHOW_HELP=false







# 
# ----------------------------------- VARIABLES -----------------------------------
# 


# Git default branch to fetch code from
GIT_BRANCH="dev"

# Local kalgudi webapp repository.
LOCAL_REPO="website"

# Backup directory in case you want local repository backup.
BACKUP_DIRECTORY="backup/"

# Android build directory (non-crosswalk)
APK_SOURCE_DIRECTORY="kalgudiLollipop"

# Android build directory (crosswalk version)
APK_SOURCE_DIRECTORY_CW="experiment"

# The exact location where source code from local repository is pasted for build
# This source directory is without crosswalk
# With crosswalk version follows below.
SOURCE_DIRECTORY=$APK_SOURCE_DIRECTORY"/www/"
SOURCE_DIRECTORY_CW=$APK_SOURCE_DIRECTORY_CW"/www/"

# APP_BASE_PATH=$APK_SOURCE_DIRECTORY"/www/app/"

# Path to www/app folder inside android build directory without crosswalk 
# with crosswalk version path follows later
APP_BASE_PATH=$APK_SOURCE_DIRECTORY"/www/app/"
APP_BASE_PATH_CW=$APK_SOURCE_DIRECTORY_CW"/www/app/"

# Path to www/app/common directory without crosswalk 
# with crosswalk version path follows later
COMMON_BASE_PATH=$APP_BASE_PATH"common/"
COMMON_BASE_PATH_CW=$APP_BASE_PATH_CW"common/"

# Path to www/app/common/images directory without crosswalk 
# with crosswalk version path follows later
IMAGES_BASE_PATH=$APP_BASE_PATH"common/images/"
IMAGES_BASE_PATH_CW=$APP_BASE_PATH_CW"common/images/"

# Path to www/buildDependency directory without crosswalk 
# with crosswalk version path follows later
BUILD_DEPENDENCY_DIRECTORY=$APK_SOURCE_DIRECTORY"/www/buildDependency"
BUILD_DEPENDENCY_DIRECTORY_CW=$APK_SOURCE_DIRECTORY_CW"/www/buildDependency"

# Path to www/webapp directory without crosswalk 
# with crosswalk version path follows later
WEBAPP_BASE_PATH=$APP_BASE_PATH"webapp"
WEBAPP_BASE_PATH_CW=$APP_BASE_PATH_CW"webapp"

# Path to www/build directory without crosswalk 
# with crosswalk version path follows later
WEBAPP_BUILD_BASE_PATH=$APP_BASE_PATH"build/"
WEBAPP_BUILD_BASE_PATH_CW=$APP_BASE_PATH_CW"build/"

# Path to apk file without crosswalk
# with crosswalk version follows later.
# NOTE: This path of apk file is of unsigned kalgudi apk.
BUILD_APK_PATH=$APK_SOURCE_DIRECTORY"/platforms/android/build/outputs/apk/android-debug.apk"
BUILD_APK_PATH_CW=$APK_SOURCE_DIRECTORY_CW"/platforms/android/build/outputs/apk/android-armv7-debug.apk"

# Path to signed apk file without crosswalk
# with crosswalk version follows later.
# NOTE: This path of apk file is of kalgudi apk.
BUILD_SIGNED_APK_PATH=$APK_SOURCE_DIRECTORY"/platforms/android/build/outputs/apk/android-release-unsigned.apk"
BUILD_SIGNED_APK_PATH_CW=$APK_SOURCE_DIRECTORY_CW"/platforms/android/build/outputs/apk/android-armv7-release-unsigned.apk"

# Path to android build directory where you align the apk after signing
ANDROID_ALIGNMENT_DIRECTORY='softwares/android-sdk-linux/build-tools/23.0.2'


# 
# ----------------------------------- ERROR CODES -----------------------------------
# 

errorCode=0

ERROR_NO_GRUNT_FOUND=1
ERROR_LOCAL_REPO_NOT_FOUND=2
ERROR_BITBUCKET_DOWN=3
ERROR_INVALID_GIT_CONFIG=4





# 
# ----------------------------------- METHODS -----------------------------------
# 



# 
# Copy latest Gruntfile from local repo to kalgudi APK build directory.
# It copies grunt file to 
# 
copyGrunt() {

    # Attempt copying of grunt file to exact location
    if $BUILD_WITH_CROSSWALK; then
        cp -f $LOCAL_REPO'/Gruntfile.js' $APK_SOURCE_DIRECTORY_CW'/Gruntfile.js'
    else
        cp -f $LOCAL_REPO'/Gruntfile.js' $APK_SOURCE_DIRECTORY'/Gruntfile.js'
    fi

    # Copy success status
    $errorCode=$?
    
    if [ $? -eq 0 ]; then
        log 'Got latest gruntfile from local repository.'
    else
        logError 'Grunt file access error.'
        pushNotification 'Build Failed: Unable to fetch latest gruntfile from local repository.'
        exit $ERROR_NO_GRUNT_FOUND
    fi;

    return "$errorCode"
}


# 
# Create backup of previous app repository. The backup is in compressed gzip format.
# The compressed backup is created in backup directory with the current timestamp as 
# file name.
# 
createPreviousApkBackup() {
    
    log 'Backing up your last successful build.'
    log 'Please be patient, backup process may take some time...'


    # Backup file name is a combination of year-month-date-hours minutes and seconds
    # with .tar.gz extension
    backupFileName=$(date +%y-%m-%d-%H%M%S)'.tar.gz'


    # Creates a compressed gzip file in $APK_SOURCE_DIRECTORY with $backFileName
    tar -zcvf $BACKUP_DIRECTORY$backupFileName $SOURCE_DIRECTORY

    $errorCode=$?

    if [ $errorCode -eq 0 ]; then
        log 'Okay I backedup your last successful build.'
    else
        logError 'Backup failed, unable to backup your last build.'
        log 'Continue without backup (y/n)'

        # Confirm to continue further from user
        read -n1 ans

        if [ "$ans" = "y" ] || [ "$ans" = "Y" ]; then
            $errorCode=0
        fi;
    fi;

    return "$errorCode"
}


# 
# Change current directory to local repository.
# On success returns 0 otherwise exits from the application.
# 
moveToLocalRepo() {
    cd $LOCAL_REPO

    if [ $? -ne 0 ]; then
        logError 'Local repository directory not found.'
        pushNotification 'Build Failed: Unable to locate repository directory.'
        exit $ERROR_LOCAL_REPO_NOT_FOUND
    fi;

    $errorCode=0

    return "$errorCode"
}


# 
# Pull the latest code from git repository.
# This function assumes that git is preconfigured in $GIT_DIRECTORY.
# It will ask for user credentials if required in the process of pulling code from git.
# By default it uses the dev branch, but is overrided if the file is run with -b <branch> option
# 
# It accepts a parameter number 0 or 1. 1 if attempting twice to get latest source code, 
# otherwise 0.
# 
# On success returns 0 otherwise exists from the application
# 
pullLatestCode() {

    # If this method was called once more
    if [ $1 -ne 1 ]; then
        log 'Updating repository with latest source.'

        moveToLocalRepo
    fi;


    git pull --rebase upstream $GIT_BRANCH

    $errorCode=$?


    if [ $? -ne 0 ]; then

        # Check bitbucket working or not
        testBitbucketConnectivity

        if [ $? -ne 0 ]; then
            logError 'Unable to connect with bitbucket. Please check your internet connectivity.'
            pushNotification 'Build Failed: Unable to connect with bitbucket. Please check your internet connectivity.'

            exit $ERROR_BITBUCKET_DOWN
        else
        
            if [ $1 -eq 1 ]; then
                # If already one attempt has been made then exit
                logError 'Something screwed up with git configurations. Unable to fetch lastest source code.'
                pushNotification 'Build Failed: Unable to get latest source code. Something screwed up with git configurations.'
                
                exit $ERROR_INVALID_GIT_CONFIG
            else
                # Bitbucket working fine, give one more attempt to pull latest code
                pullLatestCode 1
            fi;
        
        # End of checking bitbucket connectivity
        fi;

    # End of check successfull pull.
    fi;

    # Move back to root directory
    cd ../

    $errorCode=0

    return "$errorcode"
}



# 
# Copy app and build dependency directory from local repo to APK build directory.
# Calling this method will create or replace existing source in APK build directory
# if exists.
# 
copyAppCode() {
    printf '\n\n\e[1;37;42m[INFO]\e[0;97;49m : Copying latest code to APK build directory.\n'

    cp -rf $LOCAL_REPO'/app' $SOURCE_DIRECTORY
    cp -rf $LOCAL_REPO"/buildDependency" $SOURCE_DIRECTORY

    # printf '\n\e[1;37;42m[INFO]\e[0;97;49m : Successfully copied latest code to APK build directory.\n'
}



# 
# Copy app and build dependency directory from local repo to experiment build directory.
# Calling this method will create or replace existing source in APK build directory
# if exists. 
# 
# Use this method only if building APK with crosswalk.
# 
copyAppCodeToCrosswalk() {
    printf '\n\n'
    printf '\e[1;37;42m[INFO]\e[0;97;49m : Copying latest code to APK build directory.\n'

    cp -rf $LOCAL_REPO'/app' $SOURCE_DIRECTORY_CW
    cp -rf $LOCAL_REPO'/buildDependency' $SOURCE_DIRECTORY_CW

    # printf '\n\e[1;37;42m[INFO]\e[0;97;49m : Successfully copied latest code to APK build directory.\n'
}



# 
# Modifies kalgudi.properties.js file for apk production.
# 
# This function make following changes to kalgudi.properties.js file.
# -------------------------------------------------------------------
# "buildFor": true                              --> "buildFor"      : false,
# "enviorment": "devkalgudi.vasudhaika.net"     --> "enviorment"    : "kalgudi.com",
# "secure": "http://",                          --> "secure"        : "https://",
# "isProduction: false",                        --> "isProduction"  : true
# "appVersion":"1.1.228",                       --> "appVersion"    : "1.1.429",
# "plateForm":"DESKTOP"                         --> "plateForm"     : "APK"
modifyKalgudiPropertiesForProduction() {
    printf '\e[1;37;42m[INFO]\e[0;97;49m : Modifying kalgudi.properties.js file for mobile production.\n'

    if $BUILD_WITH_CROSSWALK; then
        COMMON_BASE_PATH=$APP_BASE_PATH_CW"common/"
    else
        COMMON_BASE_PATH=$APP_BASE_PATH"common/"
    fi

    PROPERTIES_FILE_PATH=$COMMON_BASE_PATH"primary/kalgudi.properties.js"

    # Modify buildFor property for mobile
    sed -i 's/"buildFor"\s*:\s*true/"buildFor": false/' $PROPERTIES_FILE_PATH

    # Modify isProduction property for mobile
    sed -i 's/"isProduction"\s*:\s*false/"isProduction": true/' $PROPERTIES_FILE_PATH

    # Modify environment property for kalgudi production
    sed -i 's/"enviorment"\s*:\s*"devkalgudi.vasudhaika.net"/"enviorment": "kalgudi.com"/' $PROPERTIES_FILE_PATH

    # Modify environment property for kalgudi production (If using IP based URL)
    sed -i 's/"enviorment"\s*:\s*"183.82.3.219:88"/"enviorment": "kalgudi.com"/' $PROPERTIES_FILE_PATH

    # Modify secure property for kalgudi production
    sed -i 's/"secure"\s*:\s*"http:\/\/"/"secure": "https:\/\/"/' $PROPERTIES_FILE_PATH

    # Modify appVersion property for mobile
    sed -i 's/"appVersion"\s*:\s*"1.1.228"/"appVersion": "1.1.429"/' $PROPERTIES_FILE_PATH
    sed -i 's/"appVersion"\s*:\s*"1.1.429"/"appVersion": "1.1.429"/' $PROPERTIES_FILE_PATH

    # Modify appVersion property for mobile
    sed -i 's/"plateForm"\s*:\s*"DESKTOP"/"plateForm": "APK"/' $PROPERTIES_FILE_PATH


    # printf '\n\e[1;37;42m[INFO]\e[0;97;49m : I have updated properties file for mobile production use.\n'
}



# 
# Modifies kalgudi.properties.js file for development apk.
# 
# This function make following changes to kalgudi.properties.js file.
# -------------------------------------------------------------------
# "buildFor": true                              --> "buildFor"      : false,
# "enviorment": "kalgudi.com"                   --> "enviorment"    : "devkalgudi.vasudhaika.net",
# "isProduction: true",                         --> "isProduction"  : false
# "secure": "https://",                         --> "secure"        : "http://",
# "appVersion":"1.1.228",                       --> "appVersion"    : "D1.1.310",
# "plateForm":"DESKTOP"                         --> "plateForm"     : "APK"
modifyKalgudiPropertiesForDevelopment() {
    printf '\e[1;37;42m[INFO]\e[0;97;49m : Modifying kalgudi.properties.js file for mobile development.\n'

    if $BUILD_WITH_CROSSWALK; then
        COMMON_BASE_PATH=$APP_BASE_PATH_CW"common/"
    else
        COMMON_BASE_PATH=$APP_BASE_PATH"common/"
    fi

    PROPERTIES_FILE_PATH=$COMMON_BASE_PATH"primary/kalgudi.properties.js"

    # Modify buildFor property for mobile
    sed -i 's/"buildFor"\s*:\s*true/"buildFor": false/' $PROPERTIES_FILE_PATH

    # Modify isProduction property for mobile
    sed -i 's/"isProduction"\s*:\s*true/"isProduction": false/' $PROPERTIES_FILE_PATH

    # Modify environment property for kalgudi production
    sed -i 's/"enviorment"\s*:\s*"kalgudi.com"/"enviorment": "183.82.3.219:88"/' $PROPERTIES_FILE_PATH

    # Modify secure property for kalgudi production
    sed -i 's/"secure"\s*:\s*"https:\/\/"/"secure": "http:\/\/"/' $PROPERTIES_FILE_PATH

    # Modify appVersion property for mobile
    sed -i 's/"appVersion"\s*:\s*"1.1.228"/"appVersion": "D1.1.310"/' $PROPERTIES_FILE_PATH
    sed -i 's/"appVersion"\s*:\s*"1.1.429"/"appVersion": "D1.1.310"/' $PROPERTIES_FILE_PATH

    # Modify appVersion property for mobile
    sed -i 's/"plateForm"\s*:\s*"DESKTOP"/"plateForm": "APK"/' $PROPERTIES_FILE_PATH


    # printf '\n\e[1;37;42m[INFO]\e[0;97;49m : I have updated properties file for mobile production use.\n'
}




# 
# Minify all js files using grunt tool.
# This function runs grunt command in APK root directory. It assumes that grunt 
# configuration and packages are present in APK root directory.
# 
runGrunt() {    
    printf '\e[1;37;42m[INFO]\e[0;97;49m : Please wait while I run tool to minify script files.\n'
    printf '\e[1;37;42m[INFO]\e[0;97;49m : Executing grunt.\n'

    # Move to APK source directory and run grunt tool
    if $BUILD_WITH_CROSSWALK; then
        cd $APK_SOURCE_DIRECTORY_CW
    else
        cd $APK_SOURCE_DIRECTORY
    fi

    grunt

    # Move back to root directory
    cd ..


    # printf '\n\e[1;37;42m[INFO]\e[0;97;49m : grunt was successful. Now all your scripts are minified.\n'
}



# 
# Cleans all webapp related files and directories.
# These files and directories are not needed for building apk.
# 
removeWebappFiles() {
    printf '\e[1;37;42m[INFO]\e[0;97;49m : No need of webapp files and directories. Please wait while I clean it.\n' 

    # Delete webapp directory
    rm -rf $WEBAPP_BASE_PATH

    # Add or remove elements from the array.
    # If there is change in list of web assets
    declare -a webAssets=( 
        $APP_BASE_PATH"gruntfile.js"
        $APP_BASE_PATH"package.json"
        $APP_BASE_PATH"package-lock.json"
        $APP_BASE_PATH"fingerprint.json"
    )

    #Delete all assets that are not required in mobile
    for asset in "${webAssets[@]}"
    do
        rm -f $asset
    done

    # Move to app directory and 
    # Delete all assets that starts with web
    printf '\e[1;37;42m[INFO]\e[0;97;49m : Deleting web assets.\n'
    cd $APP_BASE_PATH
    rm -f web*

    # Move back to root directory
    cd ../../../
    
    # Remove web minified scripts
    rm -rf $WEBAPP_BUILD_BASE_PATH

    # printf '\n\e[1;37;42m[INFO]\e[0;97;49m : Successfully removed all webapp files.\n'
}



# 
# Cleans all webapp related files and directories.
# These files and directories are not needed for building apk.
# Use this function if you are building APK with crosswalk
# 
removeWebappFilesCW() {
    printf '\e[1;37;42m[INFO]\e[0;97;49m : No need of webapp files and directories. Please wait while I clean it.\n' 

    # Delete webapp directory
    rm -rf $WEBAPP_BASE_PATH_CW

    # Add or remove elements from the array.
    # If there is change in list of web assets
    declare -a webAssets=( 
        $APP_BASE_PATH_CW"gruntfile.js"
        $APP_BASE_PATH_CW"package.json"
        $APP_BASE_PATH_CW"package-lock.json"
        $APP_BASE_PATH_CW"fingerprint.json"
    )

    #Delete all assets that are not required in mobile
    for asset in "${webAssets[@]}"
    do
        rm -f $asset
    done

    # Move to app directory and 
    # Delete all assets that starts with web
    printf '\e[1;37;42m[INFO]\e[0;97;49m : Deleting web assets.\n'
    cd $APP_BASE_PATH_CW
    rm -f web*

    # Move back to root directory
    cd ../../../
    
    # Remove web minified scripts
    rm -rf $WEBAPP_BUILD_BASE_PATH_CW

    # printf '\n\e[1;37;42m[INFO]\e[0;97;49m : Successfully removed all webapp files.\n'
}



#
# Removes images that are not needed by the mobile apk.
# 
removeWebappImages() {
    printf '\e[1;37;42m[INFO]\e[0;97;49m : Cleaning images not needed for mobile apk.\n'

    # Check if building with crosswalk
    if $BUILD_WITH_CROSSWALK; then
        IMAGES_PATH=$IMAGES_BASE_PATH_CW
    else
        IMAGES_PATH=$IMAGES_BASE_PATH
    fi


    # List of images that are not needed for mobile apk.
    # Add or remove these list if there is any change in the images folder
    declare -a imagesToDelete=( 
        $IMAGES_PATH"00-Vasudhaika.gif"
        $IMAGES_PATH"Accounts.jpg"
        $IMAGES_PATH"addscreen.png"
        $IMAGES_PATH"comingsoon.jpg"
        # $IMAGES_PATH"gmaillogo.png"  # Removed Gmail logo icon since we are using it in mobile from now.
        $IMAGES_PATH"groups_default.png"
        $IMAGES_PATH"Invitereceived.jpg"
        $IMAGES_PATH"more_images.png"
        $IMAGES_PATH"myPages.jpg"
        $IMAGES_PATH"Network.jpg"
        $IMAGES_PATH"Opportunity.jpg"
        $IMAGES_PATH"outlook_com_logo.jpg"
        $IMAGES_PATH"Post.jpg"
        $IMAGES_PATH"Profile.jpg"
        $IMAGES_PATH"QnA.jpg"
        $IMAGES_PATH"SendInvite.jpg"
        $IMAGES_PATH"Shareaupdate.jpg"
        $IMAGES_PATH"Whoviewedyourprofile.jpg"
        $IMAGES_PATH"yahoo-logo-small.jpg" 
    )


    #Delete all images in image folder
    for image in "${imagesToDelete[@]}"
    do
        # echo '[DEL]  : '$image
        rm -f $image
    done


    # Remove images backup if exists
    rm -rf $IMAGES_PATH"backup-images"

    # printf '\n\e[1;37;42m[INFO]\e[0;97;49m : Successfully swiped images not required for mobile apk.\n'
}



# 
# Remove all script files that got minified through grunt.
# This function runs a regular expression on Gruntfile.js and looks for a unique pattern 
# to identify the source files that are minified. Once identified remove all those files 
# from APK source code.
# 
removeMinifiedFiles() {

    # Check building with crosswalk before cleanup
    if $BUILD_WITH_CROSSWALK; then
        SOURCE_DIR=$APK_SOURCE_DIRECTORY_CW
    else
        SOURCE_DIR=$APK_SOURCE_DIRECTORY
    fi


    files=($(ls -l | grep -oP '([^\/*]"www\/[\w\/\.\-]*)' $SOURCE_DIR'/Gruntfile.js'))


    filesLen=$(echo ${#files[@]})

    # Move to source directory
    cd $SOURCE_DIR


    #Delete all assets that are not required in mobile
    for file in "${files[@]}"
    do
        filesLen=`expr $filesLen - 1`

        # 
        # Since above regular expression is also capturing our destination file.
        # Hence, make sure that the destination file is not deleted by loop.
        # 
        if [[ $filesLen -gt 0 ]]; then
            
            # Remove double quote if exists in filename
            file=${file/\"/}

            # Delete the file from APK source code.
            rm -rf $file
        fi
    done


    # Move back to root directory
    cd ../
}



# 
# Runs ionic/cordova to build android APK.
# 
buildAndroid() {

    printf '\e[1;37;42m[INFO]\e[0;97;49m : Please wait while I run scripts to build android APK.\n\n'

    # Move to apk build source directory
    if $BUILD_WITH_CROSSWALK; then
        cd $APK_SOURCE_DIRECTORY_CW
    else
        cd $APK_SOURCE_DIRECTORY
    fi


    # Run tool to build android apk
    cordova build android

    # Move back to root directory
    cd ../
}



# 
# Runs ionic/cordova to build android APK for release.
# 
buildAndroidForRelease() {

    printf '\e[1;37;42m[INFO]\e[0;97;49m : Please wait while I run scripts to build android APK for release.\n\n'

    # Move to apk build source directory
    if $BUILD_WITH_CROSSWALK; then
        cd $APK_SOURCE_DIRECTORY_CW
    else
        cd $APK_SOURCE_DIRECTORY
    fi

    # Run tool to build android apk
    cordova build --release android

    # Move back to root directory
    cd ../
}




# 
# Runs ionic cordova to build android signed production APK for release.
# 
buildAndroidForReleaseSigned() {

    # Build production APK first
    buildAndroidForRelease

    # Sign the production APK
    signProductionAPK

    # Align signed APK
    if $BUILD_WITH_CROSSWALK; then
        alignSignedAPKCW
    else
        alignSignedAPK
    fi
}



# 
# Sign the production build APK
# 
signProductionAPK() {

    printf '\n'
    printf '\e[1;37;42m[INFO]\e[0;97;49m : Please wait while I sign the production APK.\n'

    signedFileName="android-release-unsigned.apk"

    # Copy files based on build with crosswalk
    if $BUILD_WITH_CROSSWALK; then

        signedFileName="android-armv7-release-unsigned.apk"

        # Delete previous unsigned release APK and copy new android release APK to kalgudiLollipop directory
        rm -f $APK_SOURCE_DIRECTORY_CW'/'$signedFileName

        cp -f $BUILD_SIGNED_APK_PATH_CW $APK_SOURCE_DIRECTORY_CW'/'$signedFileName

        # Move to kalgudiloppipop directory to sign APK
        cd $APK_SOURCE_DIRECTORY_CW

    else

        # Delete previous unsigned release APK and copy new android release APK to kalgudiLollipop directory
        rm -f $APK_SOURCE_DIRECTORY'/'$signedFileName

        cp -f $BUILD_SIGNED_APK_PATH $APK_SOURCE_DIRECTORY'/'$signedFileName

        # Move to kalgudiloppipop directory to sign APK
        cd $APK_SOURCE_DIRECTORY

    fi


    # Sign production APK
    jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore kalgudiplay.keystore $signedFileName kalgudi.keystore

    # Move out of kalgudiLollipop directory after APK is signed.
    cd ../

    printf '\n'
    printf '\e[1;37;42m[INFO]\e[0;97;49m : Kalgudi production APK JAR signed successfully.\n\n'
}



# 
# Copies and alignes the signed Kalgudi APK.
# 
alignSignedAPK() {

    sourceFileName="android-release-unsigned.apk"
    destFileName="kalgudi-signed.apk"

    # Remove previous signed APK and copy new signed APK to softwares directory
    rm -f $ANDROID_ALIGNMENT_DIRECTORY'/'$sourceFileName
    rm -f $ANDROID_ALIGNMENT_DIRECTORY'/'$destFileName

    cp -f $APK_SOURCE_DIRECTORY'/'$sourceFileName $ANDROID_ALIGNMENT_DIRECTORY'/'$sourceFileName

    # Move to android alignment directory
    cd $ANDROID_ALIGNMENT_DIRECTORY

    # Run command to for zip align and create android signed without crosswalk version apk.
    ./zipalign -v 4 $sourceFileName $destFileName


    # Move to root directory
    cd ../../../../

    printf '\n'
    printf '\e[1;37;42m[INFO]\e[0;97;49m : Successfully signed and aligned Kalgudi APK without crosswalk.\n'
}



# 
# Copies and alignes the signed Kalgudi APK.
# 
alignSignedAPKCW() {

    sourceFileName="android-armv7-release-unsigned.apk"
    destFileName="kalgudi-signed-cw.apk"

    # Remove previous signed APK and copy new signed APK to softwares directory
    rm -f $ANDROID_ALIGNMENT_DIRECTORY'/'$sourceFileName
    rm -f $ANDROID_ALIGNMENT_DIRECTORY'/kalgudi-signed-cw.apk'

    # Copy apk from experiment directory to alignment directory
    cp -f $APK_SOURCE_DIRECTORY_CW'/'$sourceFileName $ANDROID_ALIGNMENT_DIRECTORY'/'$sourceFileName

    # Move to android alignment directory
    cd $ANDROID_ALIGNMENT_DIRECTORY

    # Run command to for zip align and create android signed without crosswalk version apk.
    ./zipalign -v 4 $sourceFileName $destFileName


    # Move to root directory
    cd ../../../../

    printf '\n'
    printf '\e[1;37;42m[INFO]\e[0;97;49m : Successfully signed and aligned Kalgudi APK with crosswalk.\n'
}



# 
# Copy signed production APK to kalgudi root directory.
# 
copySignedApkToRoot() {

    fileName="kalgudi-signed.apk"
    withCW="without"

    if $BUILD_WITH_CROSSWALK; then
        fileName="kalgudi-signed-cw.apk"
        withCW="with"
    fi

    # Copy signed APK to root directory
    cp $ANDROID_ALIGNMENT_DIRECTORY'/'$fileName $fileName

    curPath=$(pwd)

    printf '\n'
    printf '\e[1;37;42m[INFO]\e[0;97;49m : Successfully build Kalgudi Signed Production APK %s crosswalk at \n' $withCW
    printf '\t %s/%s\n\n' $curPath $fileName
}



# 
# Copies the APK build to root directory for easy access.
# 
copyBuildApkToRoot() {

    curPath=$(pwd)
    destFile="kalgudi-debug.apk"

    if $BUILD_WITH_CROSSWALK; then
        destFile="kalgudi-debug-with-cw.apk"
        cp $BUILD_APK_PATH_CW $destFile
    else
        cp $BUILD_APK_PATH $destFile
    fi

    printf '\n\n\e[1;37;42m[INFO]\e[0;97;49m : Android APK build success at %s/%s\n\n' $curPath $destFile

}



# 
# Build APK with crosswalk
# 
buildWithCrosswalk() {

    # Do all tasks one by one
    if "$IS_BACKUP_REQUIRED" = true
    then
        createPreviousApkBackup
    fi
    
    if "$PULL_FROM_GIT" = true
    then
        pullLatestCode
    fi

    copyAppCodeToCrosswalk
    

    if "$BUILD_FOR_PRODUCTION" = true
    then
        modifyKalgudiPropertiesForProduction
    else
        modifyKalgudiPropertiesForDevelopment
    fi
    
    copyGrunt

    runGrunt
    
    removeWebappFilesCW
    
    removeWebappImages

    removeMinifiedFiles

    # Notify user about modifying mobile index file.
    printf '\n\n\e[1;37;41m[WARN]\e[0;97;49m : Please modify mobile_index.html before building APK.\n'
    printf '\n\e[1;37;41m[WARN]\e[0;97;49m : Have you modified mobile_index.html file? (y/n): '

    read -n1 ans

    printf '\n'

    if [ $ans == 'y' ] || [ $ans == 'Y' ]
    then
        if $BUILD_SIGNED_APK; then
            printf '\n'
            printf '\e[1;37;41m[WARN]\e[0;97;49m : Have you modified build version in experiment directory? (y/n): '

            read -n1 ans
            
            printf '\n'

            if [ $ans == 'y' ] || [ $ans == 'Y' ] 
            then
                echo ''
                buildAndroidForReleaseSigned

                copySignedApkToRoot
            else
                printf '\n\e[1;37;41m[EXIT]\e[0;97;49m : Please first modify mobile build version and re-run the script.\n\n'
            fi
            # End of build signed apk

        else
            buildAndroid
            
            copyBuildApkToRoot
        fi
        
    else
        printf '\n\e[1;37;41m[EXIT]\e[0;97;49m : Please first modify mobile_index.html file and re-run the script.\n\n'
    fi
}


# 
# Normal APK build without crosswalk
# 
build() {

    # Do all tasks one by one
    if "$IS_BACKUP_REQUIRED" = true
    then
        createPreviousApkBackup
    fi
    
    if "$PULL_FROM_GIT" = true
    then
        pullLatestCode
    fi

    copyAppCode
    

    if "$BUILD_FOR_PRODUCTION" = true
    then
        modifyKalgudiPropertiesForProduction
    else
        modifyKalgudiPropertiesForDevelopment
    fi
    
    copyGrunt

    runGrunt
    
    removeWebappFiles
    
    removeWebappImages

    removeMinifiedFiles

    # Notify user about modifying mobile index file.
    printf '\n\n\e[1;37;41m[WARN]\e[0;97;49m : Please modify mobile_index.html before building APK.\n'
    printf '\n\e[1;37;41m[WARN]\e[0;97;49m : Have you modified mobile_index.html file? (y/n): '

    read -n1 ans

    printf '\n'

    if [ $ans == 'y' ] || [ $ans == 'Y' ]
    then
        if "$BUILD_SIGNED_APK" = true
        then
            printf '\n'
            printf '\e[1;37;41m[WARN]\e[0;97;49m : Have you modified build version in kalgudiLollipop directory? (y/n): '

            read -n1 ans
            
            printf '\n'

            if [ $ans == 'y' ] || [ $ans == 'Y' ] 
            then
                buildAndroidForReleaseSigned

                copySignedApkToRoot
            else
                printf '\n\e[1;37;41m[EXIT]\e[0;97;49m : Please first modify mobile build version and re-run the script.\n\n'
            fi
            # End of build signed apk

        else
            buildAndroid
            
            copyBuildApkToRoot
        fi
        
    else
        printf '\n\e[1;37;41m[EXIT]\e[0;97;49m : Please first modify mobile_index.html file and re-run the script.\n\n'
    fi
}



# 
# Show help and about box to user.
# 
showHelp() {
    printf '\n'
    printf '[USAGE]\n'
    printf '\t./build.sh [OPTIONS]\n'
    printf '\n'
    
    printf '[OPTIONS LIST]\n'
    printf '\t%s       Store backup of previous successful APK build.\n'                    "-b"
    printf '\t%s       Pull latest code from git.\n'                                        "-g"
    printf '\t%s       Build APK with crosswalk.\n'                                         "-c"
    printf '\t%s       Build Kalgudi APK for development (Default).\n'                      "-d"
    printf '\t%s       Build Kalgudi APK for production.\n'                                 "-p"
    printf '\t%s       Build signed production APK.\n'                                      "-s"
    printf '\t%s       Note: This option can only be used if you have specified -p.\n'      "  "
    printf '\t%s             You cannot build development signed APK.\n\n'                  "  "
    printf '\t%s   Shows usage information.\n\n\n'                                          "--help"

    printf '[EXAMPLE]\n'
    printf '\t./build.sh -g -d \t %s\n'         "Pull latest code and build for development."
    printf '\t./build.sh -g -d -c \t %s\n\n'    "Same as above but with crosswalk."

    printf '\t./build.sh -d \t\t %s\n'          "Build for development without latest code."
    printf '\t./build.sh -d -c \t %s\n\n'       "Build for development with crosswalk."

    printf '\t./build.sh -g -p \t %s\n'         "Pull latest code and build for production."
    printf '\t./build.sh -g -p -c \t %s\n\n'    "Same as above but with crosswalk."

    printf '\t./build.sh -p \t\t %s\n'          "Build signed production without latest code "
    printf '\t              \t\t %s\n\n'        "and crosswalk."

    printf '\t./build.sh -g -p -s \t %s\n'      "Build production signed APK without crosswalk."
    printf '\t./build.sh -g -p -s -c \t %s\n'   "Build production signed APK with crosswalk."
    printf '\t./build.sh -p -s \t %s\n'         "Build production signed APK without latest code"
    printf '\t                 \t %s\n'         "and crosswalk."

    printf '\n\n'
}



# 
# Sets various default options required to run the script.
# 
setDefaults() {
    if [[ $1 = '-d' ]]; then            # -d option is used to build apk for development
        BUILD_FOR_PRODUCTION=false

    elif [[ $1 = '-p' ]]; then          # -p option is used to build apk for production
        BUILD_FOR_PRODUCTION=true

    elif [[ $1 = '-c' ]]; then          # -c option is used to build with crosswalk
        BUILD_WITH_CROSSWALK=true

    elif [[ $1 = '-s' ]]; then          # -s option is used to build signed apk
        BUILD_SIGNED_APK=true

    elif [[ $1 = '-b' ]]; then          # -b option is used to take backup of previous success APK build directory
        IS_BACKUP_REQUIRED=true

    elif [[ $1 = '-g' ]]; then          # -g option is used to pull latest code from git
        PULL_FROM_GIT=true

    elif [[ $1 = '--help' ]]; then
        SHOW_HELP=true

    else
        printf '\n\e[1;37;41m[WARN]\e[0;97;49m : Invalid option.\n'
        SHOW_HELP=true
    fi
}



# 
# Initializes configurations needed to run the script.
# It checks for parameters given by user and overrides script default properties 
# as mentioned by parameters.
# 
init() {

    if [[ $# -ge 1 ]]; then
        setDefaults $1
    fi

    if [[ $# -ge 2 ]]; then
        setDefaults $2
    fi

    if [[ $# -ge 3 ]]; then
        setDefaults $3
    fi

    if [[ $# -ge 4 ]]; then
        setDefaults $4
    fi

    if [[ $# -ge 5 ]]; then
        setDefaults $5
    fi

    if [[ $# -ge 6 ]]; then
        setDefaults $6
    fi
}





##
# Send notification to ubuntu system. By defualt it sends show notification for
# 10 seconds. 
# It accepts a parameter i.e. the message to show within notification.
# 
pushNotification() {
    notify-send -t 1800000 -i ~/Documents/scripts/jarvis/assets/ai.png 'Jarvis - Apk-noW' "$1"
}


##
# Prints error message on console.
# It accepts a parameter i.e. the message to show.
# 
logError() {
    printf "\n\e[1;37;41m[WARN]\e[0;97;49m : $1\n\n"
}


##
# Prints a message on console.
# It accepts a parameter i.e. the message to show.
# 
log() {
    printf "\n\n\e[1;37;42m[INFO]\e[0;97;49m : $1\n"
}


##
# Tests internet connectivity. The function will call system
# ping command for 4 times to check internet connectivity issue 
# with bitbucket.
# 
# Returns 0 if ping was successful.
# 
testBitbucketConnectivity() {

    log 'Testing internet connectivity. Please wait...'

    ping -c 4 https://bitbucket.org

    return "$?"
}




# 
# The main driver function of whole script.
# 
main() {

    # Deprecated the old welcome screen
    # Notify users about the script
    # Please credit developer time and effort
    # printf '\n\e[1;31m'
    # printf '+------------------------------------------------------------------------------+\n'
    # printf '|                             ANDROID BUILDER v1                               |\n'
    # printf '|                                           (c) Vasudhaika Software Sols.      |\n' 
    # printf '|                                               developed by - Pankaj Prakash  |\n' 
    # printf '+------------------------------------------------------------------------------+\n'
    # printf '\e[0;97;49m\n'


    # Initialize all default settings before running application
    init $1 $2 $3 $4 $5 $6


    if "$SHOW_HELP" = true 
    then
        showHelp
    else
        if "$BUILD_WITH_CROSSWALK" = true
        then
            buildWithCrosswalk
        else
            build
        fi
    fi

}








# -------------------------- Nothing beyond -------------------------- #

# Clear the board
clear

# Black background with red foreground
printf '\e[30;31m'
cat << "EOF"

       d8888          888                              888       888 
      d88888          888                              888   o   888 
     d88P888          888                              888  d8b  888 
    d88P 888 88888b.  888  888       88888b.   .d88b.  888 d888b 888 
   d88P  888 888 "88b 888 .88P       888 "88b d88""88b 888d88888b888 
  d88P   888 888  888 888888K 888888 888  888 888  888 88888P Y88888 
 d8888888888 888 d88P 888 "88b       888  888 Y88..88P 8888P   Y8888 
d88P     888 88888P"  888  888       888  888  "Y88P"  888P     Y888.v2 
             888                                                    
             888                                  (c) Pankaj Prakash
             888                                                    
                                                        
EOF

# Normal background and foreground
printf '\e[0;97;49m'

# Drive the script through with the command line paramaters provided
main $1 $2 $3 $4 $5 $6 

# Happy coding ;)
