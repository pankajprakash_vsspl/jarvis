deploy() {
    
    ROOT=`pwd`

    # Move to website
    cd ~/Documents/vsspl/website/

    WEBSITE_DIR=`pwd`

    log 'Pulling latest code from bitbucket'

    # Take latest code in website 
    git pull --rebase upstream dev

    if [ $? -ne 0 ]; then
        logError 'Unable to pull code from bitbucket, terminating'
        pushNotification 'Docker environment setup failed'
        exit 1
    fi

    # Kalgudi properties relative file path from website
    # PROPERTIES_FILE_PATH=$WEBSITE_DIR'/app/common/primary/kalgudi.properties.js'

    log 'Cleaning docker '$1' directory.'

    # Delete remote files
    sshpass -p 'vsspl123' ssh -o StrictHostKeyChecking=no docker@192.168.1.65 'rm -rf /home/docker/dockervol/uss-jetty/kalgudi-uss/'$1'/*'


    # Modify kalgudi properties for dev
    # sed -i 's/"isProduction"\s*:\s*false/"isProduction": true/' $PROPERTIES_FILE_PATH
    # sed -i 's/"enviorment"\s*:\s*ENV_PROD/"enviorment": ENV_DEV/' $PROPERTIES_FILE_PATH
    # sed -i 's/"secure"\s*:\s*"https:\/\/"/"secure": "http:\/\/"/' $PROPERTIES_FILE_PATH

    log 'Copying files from local repo to '$1

    # Copy files from local to dev
    sshpass -p 'vsspl123' scp -r /home/pankaj/Documents/vsspl/website/app/* docker@192.168.1.65:/home/docker/dockervol/uss-jetty/kalgudi-uss/$1/

    log 'Deleting unnecessary build directory for development.'

    # Delete build directory
    sshpass -p 'vsspl123' ssh -o StrictHostKeyChecking=no docker@192.168.1.65 'rm -rf /home/docker/dockervol/uss-jetty/kalgudi-uss/'$1'/build'

    # Stash the changes in local git website repo
    # git stash


    # Move to root dir
    cd $ROOT

    log 'All done.'

    pushNotification 'All done sir, made docker '$1' environment ready.'
}


##
# Prints a message on console.
# It accepts a parameter i.e. the message to show.
# 
log() {
    printf "\n\n\e[1;37;42m[INFO]\e[0;97;49m : $1\n"
}


##
# Prints error message on console.
# It accepts a parameter i.e. the message to show.
# 
logError() {
    printf "\n\e[1;37;41m[WARN]\e[0;97;49m : $1\n\n"
}



##
# Send notification to ubuntu system. By defualt it sends show notification for
# 10 seconds. 
# It accepts a parameter i.e. the message to show within notification.
# 
pushNotification() {
    notify-send -t 1800000 -i ~/Documents/scripts/jarvis/assets/ai.png 'Jarvis - Dashboard' "$1"
}


deploy $1
