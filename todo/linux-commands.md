## List of linux commands

1. _Working with files_
    * pwd
    * ls
    * cd
        * Wildcard characters
    * cp
    * mv
    * rm
    * ln
    * mkdir
    * rmdir
    * less
    * more
    * touch
    * cmp
    * comm
    * cpio
    * sdiff
    * wc
2. *Shell file editors*
    * cat
    * nano
    * vi
    * vim
3. *Zip file manipulation*
    * tar
    * zip
    * unzip
    * bzip2
4. _Search_
    * locate
    * find
5. _Command helpers_
    * type
    * which
    * whereis
    * help
    * man
6. _IO redirection_
7. _Pipelining_
8. _Filters_
    * sort
    * uniq
    * grep
    * sed
    * awk
    * fmt
    * pr
    * head
    * tail
    * tr
9. _Expansion with echo_
10. _Permissions_
    * chmod
    * su
    * sudo
    * chown
    * chgrp
11. _Memory, Process and Job control_
    * ps
    * kill
    * killall
    * pkill
    * jobs
    * bg
    * fg
    * cron
    * crontab
    * top
    * vmstat
12. *Disk utility*
    * df
    * du
    * free
    * lsblk
13. *System admin*
    * env
    * id
    * uname
    * who
    * whoami
    * who am i
    * passwd
    * users
    * user
    * init
    * reboot
    * shutdown
    * halt
    * exit
    * last
    * service
    * date
    * cal
    * lsof
    * lsmod
    * history
    * clear
    * pr
    * apt
    * mail
    * alias
    * env
    * eval
    * exec
    * mount
    * unmount
    * uptime
    * cat test.json | python -m json.tool
    * dd (dd if=/home/user/Downloads/debian.iso of=/dev/sdb1 bs=512M; sync)
    * mkpasswd
    * chage -l username
    * dpkg
14. *Networking*
    * host
    * hostname
    * ifconfig (ip addr, ip link)
    * iwconfig
    * vconfig (ip link)
    * system-config-network
    * ifup
    * ifdown
    * ping
    * traceroute
    * tracepath
    * dig
    * mtr
    * nslookup
    * netstat (ss)
    * route (ip route)
    * dhclient
    * iptables
    * curl
    * wget
    * ftp/sftp
    * ssh
    * scp
    * w
    * nmap
    * arp (ip neigh)
    * iptunnel (ip tunnel)
    * ipmaddr (ip maddr)
15. *Hardware commands*
    * lspci
    * lssci
    * lsusb
    * lscpu
16. *Misc commands*
    * !! (Repeat previous command)

**References**
- http://linuxcommand.org
- https://maker.pro/linux/tutorial/basic-linux-commands-for-beginners
- https://www.guru99.com/must-know-linux-commands.html
- https://searchdatacenter.techtarget.com/tutorial/77-Linux-commands-and-utilities-youll-actually-use
- https://www.learnenough.com/command-line-tutorial/basics
- https://www.makeuseof.com/tag/an-a-z-of-linux-40-essential-commands-you-should-know/
- https://geekflare.com/linux-networking-commands/
- https://www.tecmint.com/linux-network-configuration-and-troubleshooting-commands/
- https://itsfoss.com/basic-linux-networking-commands/
- http://www.linuxandubuntu.com/home/10-essential-linux-network-commands
- https://likegeeks.com/linux-network-commands/
- http://xahlee.info/linux/linux_networking_commands.html
- https://opensource.com/article/17/7/20-sysadmin-commands
- https://www.tecmint.com/useful-linux-commands-for-system-administrators/
- https://www.tecmint.com/useful-linux-commands-for-newbies/
- https://www.tecmint.com/20-advanced-commands-for-linux-experts/
- https://geekflare.com/linux-commands-for-system-admin/
