const data = [
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Confectioneries & Sweets",
            "subCategory2": "Indian Sweets",
            "subCategory3": "Speciality Sweets"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Confectioneries & Sweets",
            "subCategory2": "Speciality Indian Sweets",
            "subCategory3": "Boondi Chikki"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Confectioneries & Sweets",
            "subCategory2": "Speciality Indian Sweets",
            "subCategory3": "Cashew Chikki"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Confectioneries & Sweets",
            "subCategory2": "Speciality Indian Sweets",
            "subCategory3": "Coconut Laddu"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Confectioneries & Sweets",
            "subCategory2": "Speciality Indian Sweets",
            "subCategory3": "Gormiti"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Confectioneries & Sweets",
            "subCategory2": "Speciality Indian Sweets",
            "subCategory3": "Nuvvula /Til Laddu"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Confectioneries & Sweets",
            "subCategory2": "Speciality Indian Sweets",
            "subCategory3": "Peanut Chikki"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Confectioneries & Sweets",
            "subCategory2": "Speciality Indian Sweets",
            "subCategory3": "Sugar Coated Gavvalu"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Dry Fruits & Nuts",
            "subCategory3": "Raisins"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Edible Oils",
            "subCategory3": "Ghee (Clarified Butter)"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Edible Oils",
            "subCategory3": "Safflower Oil"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Flours & Rawa",
            "subCategory3": "Finger Millet Flour"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Flours & Rawa",
            "subCategory3": "Foxtail Millet Flour"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Flours & Rawa",
            "subCategory3": "Foxtail Millet Idly Rawa"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Flours & Rawa",
            "subCategory3": "Foxtail Millet Rawa"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Flours & Rawa",
            "subCategory3": "Little Millet Flour"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Flours & Rawa",
            "subCategory3": "Mixed Protein Idly Rawa"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Flours & Rawa",
            "subCategory3": "Multigrain Powder"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Flours & Rawa",
            "subCategory3": "Pearl Millet Flour"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Flours & Rawa",
            "subCategory3": "Pearl Millet Rawa"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Flours & Rawa",
            "subCategory3": "Sorghum Flour"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Flours & Rawa",
            "subCategory3": "Sorghum Idly Rawa"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Flours & Rawa",
            "subCategory3": "Sorghum Rawa"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Flours & Rawa",
            "subCategory3": "Upma Rawa"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Fryums & Papads",
            "subCategory3": "Fryums"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Fryums & Papads",
            "subCategory3": "Papad"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Fryums & Papads",
            "subCategory3": "Papads"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Grains & Millets",
            "subCategory3": "Assorted Millets"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Grains & Millets",
            "subCategory3": "Barnyard Millets"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Grains & Millets",
            "subCategory3": "Finger Millets"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Grains & Millets",
            "subCategory3": "Foxtail Millet Rice"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Grains & Millets",
            "subCategory3": "Foxtail Millets"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Grains & Millets",
            "subCategory3": "Kodo Millets"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Grains & Millets",
            "subCategory3": "Little Millets"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Grains & Millets",
            "subCategory3": "Proso Millets"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Grains & Millets",
            "subCategory3": "Sorghum Millets"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Packaged Food",
            "subCategory3": "Ready to Cook"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Powdered Drink Mixes"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Powdered Drink Mixes",
            "subCategory3": "Multigrain Powders"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Powdered Drink Mixes",
            "subCategory3": "Ragi Malt"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Powdered Spices & Masalas",
            "subCategory3": "Spice Powders"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Pulses",
            "subCategory3": "Black Gram"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Pulses",
            "subCategory3": "Horse Gram"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Speciality Food",
            "subCategory3": "Dried Jujube Fruit"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Speciality Food",
            "subCategory3": "Fruit Jelly"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Syrups, Sugars & Sweeteners",
            "subCategory3": "Honey"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Syrups, Sugars & Sweeteners",
            "subCategory3": "Jaggery"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Grocery & Gourmet",
            "subCategory2": "Syrups, Sugars & Sweeteners",
            "subCategory3": "Palm Jaggery"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Assorted Biscuits"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Coconut Biscuits"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Finger Millet Biscuits"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Foxtail Millet Biscuits"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Jonna (Sorghum) Biscuits"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Korra (Foxtail Millet) Biscuits"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Multi Millet Biscuits"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Multigrain Biscuits"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Pearl Millet Biscuits"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Ragi (Finger Millet ) Biscuits"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Ragi (Finger Millet) Biscuits"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Ragi(Finger Millet ) Biscuits"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Rusk"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Sajja (Pearl Millet) Biscuits"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Biscuits",
            "subCategory3": "Sorghum Biscuits"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Cakes",
            "subCategory3": "Jonna (Sorghum) Cake"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Cakes",
            "subCategory3": "Korra (Foxtail Millet) Cake"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Cakes",
            "subCategory3": "Ragi (Finger Millet ) Cake"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Mixture",
            "subCategory3": "Foxtail Millet Mixture"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Assorted Murukku"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Chakkalu/Nippatu"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Chakralu (Mixture)"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Chegodilu"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Chekkalu"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Finger Millet Boondi"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Finger Millet Mixture"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Foxtail Millet Boondi"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Foxtail Millet Murukku"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Karapusa/Sev"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Mixture"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Murukku"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Pearl Millet Murukku"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Ragi (Finger Millet) Murukku"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Ribbon Pakodi"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Sorghum Flakes"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Sorghum Murukku"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Spiced Nuts"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Indian Snacks & Savouries",
            "subCategory2": "Speciality Snacks",
            "subCategory3": "Wheat Puffs"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Non Veg Pickles",
            "subCategory3": "Assorted Non Veg Pickles"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Non Veg Pickles",
            "subCategory3": "Chicken Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Non Veg Pickles",
            "subCategory3": "Fish Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Non Veg Pickles",
            "subCategory3": "Mutton Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Non Veg Pickles",
            "subCategory3": "Prawn Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Amla Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Bitter Gourd Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Drumstick Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Garlic Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Ginger Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Gongura Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Green Tamarind Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Ivy Gourd pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Lemon Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Magaya (Mango) Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Mango Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Mixed Vegetable Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Mushroom Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Red Chili Gongura Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Red Chili Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Tomato Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Chutneys",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Yam Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Preserves",
            "subCategory2": "Non Veg Pickles",
            "subCategory3": "Chicken Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Preserves",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Amla Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Preserves",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Brinjal Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Preserves",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Ginger Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Preserves",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Gongura Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Preserves",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Lemon Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Preserves",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Mango Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Preserves",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Red Chili Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Preserves",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Tamarind Pickle"
        },
        {
            "mainCategory": "FOOD_PRODUCTS",
            "subCategory1": "Pickles & Preserves",
            "subCategory2": "Vegetarian Pickles",
            "subCategory3": "Tapioca Pickle"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Bamboo",
            "subCategory2": "Home & Decor",
            "subCategory3": "Flower Vases"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Bamboo",
            "subCategory2": "Home & Decor",
            "subCategory3": "Storage & Organisation"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Brass",
            "subCategory2": "Dhokra Art",
            "subCategory3": "Home Decor Accents"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Brass",
            "subCategory2": "Home & Decor",
            "subCategory3": "Storage & Organisation"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Copper",
            "subCategory2": "Home & Decor",
            "subCategory3": "Religious Idols"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Fabric",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Diaper Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Fabric",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Gift Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Fabric",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Hand Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Fabric",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Handbags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Fabric",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Lunch Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Fabric",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Multipurpose Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Fabric",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Pouches"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Fabric",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Purses & Clutches"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Fabric",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Saree Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Fabric",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "School Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Fabric",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Sling Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Fabric",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Storage & Organisation"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Fabric",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Travel Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Fabric",
            "subCategory2": "Toys & Games",
            "subCategory3": "Toys"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Grass",
            "subCategory2": "Home & Decor",
            "subCategory3": "Home Decor Accents"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Grass",
            "subCategory2": "Kitchen & Dining",
            "subCategory3": "Lunch Box"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Home & Decor",
            "subCategory2": "Home Decor Accents"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Home & Decor",
            "subCategory2": "Home Decor Accents",
            "subCategory3": "Flower Vases"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Home & Decor",
            "subCategory2": "Home Decor Accents",
            "subCategory3": "Religious Idols"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Home & Decor",
            "subCategory2": "Home Decor Accents",
            "subCategory3": "Thoran (Door Decor)"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Home & Decor",
            "subCategory2": "Paintings",
            "subCategory3": "Batik Paintings"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Home & Decor",
            "subCategory2": "Paintings",
            "subCategory3": "Kalamkari Paintings"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Home & Decor",
            "subCategory2": "Wall Decor",
            "subCategory3": "Decorative Plates"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Home & Kitchen",
            "subCategory2": "Puja Articles",
            "subCategory3": "Turmeric & Kumkum Platter"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Bottle Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Conference bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Gift Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Hand Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Handbags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Laptop Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Lunch Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Multipurpose Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Pouches"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Purses & Clutches"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Sling Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Student Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Travel Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Bags-Wallets-Luggage",
            "subCategory3": "Water Bottle Bags"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Home & Decor",
            "subCategory3": "Storage & Organisation"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Kitchen & Dining",
            "subCategory3": "Coasters"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Stationery & Office Products",
            "subCategory3": "Files & Folders"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Jute",
            "subCategory2": "Storage & Organisation",
            "subCategory3": "Wall Hangers"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Leather",
            "subCategory2": "Andhra Pradesh Leather Puppetry",
            "subCategory3": "Lamps & Lanterns"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Palm Leaf",
            "subCategory2": "Children's Accessories",
            "subCategory3": "Caps & Hats"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Palm Leaf",
            "subCategory2": "Home & Decor",
            "subCategory3": "Flower Vases"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Palm Leaf",
            "subCategory2": "Home & Decor",
            "subCategory3": "Garlands"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Palm Leaf",
            "subCategory2": "Home & Kitchen",
            "subCategory3": "Fruit Baskets"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Palm Leaf",
            "subCategory2": "Home & Kitchen",
            "subCategory3": "Gift Boxes"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Palm Leaf",
            "subCategory2": "Home & Kitchen",
            "subCategory3": "Laundry Baskets"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Palm Leaf",
            "subCategory2": "Home & Kitchen",
            "subCategory3": "Letter Box"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Palm Leaf",
            "subCategory2": "Home & Kitchen",
            "subCategory3": "Multipurpose Baskets"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Palm Leaf",
            "subCategory2": "Home & Kitchen",
            "subCategory3": "Puja Baskets"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Palm Leaf",
            "subCategory2": "Home & Kitchen",
            "subCategory3": "Storage Baskets"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Palm Leaf",
            "subCategory2": "Home & Kitchen",
            "subCategory3": "Trays"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Palm Leaf",
            "subCategory2": "Men's Accessories",
            "subCategory3": "Caps & Hats"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Palm Leaf",
            "subCategory2": "Women's Accessories",
            "subCategory3": "Caps & Hats"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Paper",
            "subCategory2": "Arts & Crafts",
            "subCategory3": "Quilling"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Seashell",
            "subCategory2": "Home & Decor",
            "subCategory3": "Bed Lamps"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Seashell",
            "subCategory2": "Home & Decor",
            "subCategory3": "Curtains"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Seashell",
            "subCategory2": "Home & Decor",
            "subCategory3": "Home Decor Accents"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Seashell",
            "subCategory2": "Home & Decor",
            "subCategory3": "Mirrors"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Seashell",
            "subCategory2": "Home Decor Accents",
            "subCategory3": "Conch Shells"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Seashell",
            "subCategory2": "Storage & Organisation",
            "subCategory3": "Keychains"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Terracotta",
            "subCategory2": "Coin Bank"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Terracotta",
            "subCategory2": "Home & Decor",
            "subCategory3": "Flower Pots"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Terracotta",
            "subCategory2": "Home & Decor",
            "subCategory3": "Flower Vases"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Terracotta",
            "subCategory2": "Home & Decor",
            "subCategory3": "Home Decor Accents"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Terracotta",
            "subCategory2": "Home & Decor",
            "subCategory3": "Lamps & Lanterns"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Terracotta",
            "subCategory2": "Home & Decor",
            "subCategory3": "Religious Idols"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Terracotta",
            "subCategory2": "Home & Decor",
            "subCategory3": "Thoran (Door Decor)"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Terracotta",
            "subCategory2": "Home & Decor",
            "subCategory3": "Wind Chimes"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Terracotta",
            "subCategory2": "Home & Kitchen",
            "subCategory3": "Cookware"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Terracotta",
            "subCategory2": "Home & Kitchen",
            "subCategory3": "Tea cups"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Terracotta",
            "subCategory2": "Home & Kitchen",
            "subCategory3": "Water Bottles"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Terracotta",
            "subCategory2": "Jugs & Pitchers",
            "subCategory3": "Terracotta Jugs"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Terracotta",
            "subCategory2": "Office Products",
            "subCategory3": "Pen Stands"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Children's Toys",
            "subCategory3": "Toys"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Etikoppaka",
            "subCategory3": "Dolls"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Etikoppaka",
            "subCategory3": "Flower Vases"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Etikoppaka",
            "subCategory3": "Home Decor Accents"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Etikoppaka",
            "subCategory3": "Jars & Containers"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Etikoppaka",
            "subCategory3": "Mobile Phone Holders"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Etikoppaka",
            "subCategory3": "Pen Stands"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Etikoppaka",
            "subCategory3": "Religious Idols"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Etikoppaka",
            "subCategory3": "Toys"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Hair Accessories",
            "subCategory3": "Hair Clips"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Home & Decor",
            "subCategory3": "Clocks"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Home & Decor",
            "subCategory3": "Curtain Rods"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Home & Decor",
            "subCategory3": "Flower Vases"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Home & Decor",
            "subCategory3": "Home Decor Accents"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Home & Decor",
            "subCategory3": "Pen Stands"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Home & Decor",
            "subCategory3": "Religious Idols"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Home & Decor",
            "subCategory3": "Sculptured Idols"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Home & Decor",
            "subCategory3": "Storage & Organisation"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Home & Decor",
            "subCategory3": "Wall Brackets"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Kitchen & Dining",
            "subCategory3": "Combs"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Kitchen & Dining",
            "subCategory3": "Table Placemats"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Kitchen & Dining",
            "subCategory3": "Tableware & Cutlery"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Kitchen & Dining",
            "subCategory3": "Trays"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Kondapalli",
            "subCategory3": "Home Decor Accents"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Kondapalli",
            "subCategory3": "Religious Idols"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Lacquerware",
            "subCategory3": "Home Decor Accents"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Lacquerware",
            "subCategory3": "Jars & Containers"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Lacquerware",
            "subCategory3": "Pen Stands"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Lacquerware",
            "subCategory3": "Religious Idols"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Lacquerware",
            "subCategory3": "Stationery"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Lacquerware",
            "subCategory3": "Teethers"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Lacquerware",
            "subCategory3": "Toys"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Learning & Education",
            "subCategory3": "Toys"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Office Products",
            "subCategory3": "Card Holders"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Office Products",
            "subCategory3": "Pen Stands"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Sculptures",
            "subCategory3": "Home Decor Accents"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Sculptures",
            "subCategory3": "Religious Idols"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Stationery & Office Products",
            "subCategory3": "Desk Accessories & Storage Products"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Cutlery & Crafts",
            "subCategory3": "Combs"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Cutlery & Crafts",
            "subCategory3": "Home Decor Accents"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Cutlery & Crafts",
            "subCategory3": "Jars & Containers"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Cutlery & Crafts",
            "subCategory3": "Lamps & Lanterns"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Cutlery & Crafts",
            "subCategory3": "Pen stand"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Cutlery & Crafts",
            "subCategory3": "Photo Frames"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Cutlery & Crafts",
            "subCategory3": "Table Placemats"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Cutlery & Crafts",
            "subCategory3": "Toys"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Cutlery & Crafts",
            "subCategory3": "Trays"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Wooden Cutlery & Crafts",
            "subCategory3": "Combs"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Wooden Cutlery & Crafts",
            "subCategory3": "Flower Vases"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Wooden Cutlery & Crafts",
            "subCategory3": "Home Decor Accents"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Wooden Cutlery & Crafts",
            "subCategory3": "Pen Stands"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Wooden Cutlery & Crafts",
            "subCategory3": "Pens"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Wooden Cutlery & Crafts",
            "subCategory3": "Tableware & Cutlery"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Wooden Cutlery & Crafts",
            "subCategory3": "Toys"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Wood",
            "subCategory2": "Udayagiri Wooden Cutlery & Crafts",
            "subCategory3": "Trays"
        },
        {
            "mainCategory": "HANDICRAFTS",
            "subCategory1": "Woollen",
            "subCategory2": "Home Decor accents",
            "subCategory3": "Thoran (Door Decor)"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Childrens Accessories",
            "subCategory2": "Hair Accessories",
            "subCategory3": "Hairbands"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Aches & Pains"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Acidity"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Amla Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Amruthavalli Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Ashwagandha Churna"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Athimadhuram Churna"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Constipation"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Cough"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Diabetes"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Diabetic Churna"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Haritaki (Karakaya) Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Kidney Stones"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Piles"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Pippali Churna"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Sunamukhi Churna"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Tamarind seeds Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Triphala Churna"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Tulsi Churna"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Alternative Medicine",
            "subCategory3": "Weight loss"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Amla Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Bath Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Bhringraj Leaves Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Body Scrubbers"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Face Packs"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Hair Masks"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Hair Oil"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Henna Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Hibiscus Flower Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Hibiscus Leaves Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Kasturi Turmeric Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Multani Mitti (Fullers Earth)"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Neem Leaves Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Orange Peels Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Rose Petals Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Shampoo Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Shikakai Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Soap"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Soapnut Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Turmeric Powder"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Herbal Products",
            "subCategory2": "Home Fragrance",
            "subCategory3": "Incense sticks"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Home & Kitchen",
            "subCategory2": "Puja Articles",
            "subCategory3": "Kumkum"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Pain Relief Products",
            "subCategory2": "Massage Tools"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Perfumes & Fragrances",
            "subCategory2": "Fragrances",
            "subCategory3": "Roll On Fragrance"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Terracotta",
            "subCategory2": "Body, Skin & Hair Care",
            "subCategory3": "Foot Scrubbers"
        },
        {
            "mainCategory": "HEALTH & PERSONAL CARE",
            "subCategory1": "Women's Hygiene",
            "subCategory2": "Sanitary Napkins"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Herbal Products",
            "subCategory2": "Home Fragrance",
            "subCategory3": "Incense Sticks"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Home & Decor",
            "subCategory2": "Home Decor Accents",
            "subCategory3": "Artificial Flowers"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Home & Decor",
            "subCategory2": "Home Decor Accents",
            "subCategory3": "Garlands"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Home & Decor",
            "subCategory2": "Home Decor Accents",
            "subCategory3": "Lamp shades"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Home & Decor",
            "subCategory2": "Home Fragrance",
            "subCategory3": "Incense Sticks"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Cookware",
            "subCategory3": "Tawa"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Drinkware",
            "subCategory3": "Coffee Mugs"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Drinkware",
            "subCategory3": "Copper Tumblers"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Drinkware",
            "subCategory3": "Earthen Tumblers"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Drinkware",
            "subCategory3": "Tea Cups"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Drinkware",
            "subCategory3": "Water Bottles"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Jugs & Pitchers",
            "subCategory3": "Copper Jugs"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Jugs & Pitchers",
            "subCategory3": "Terracotta Jugs"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Kitchen Tools",
            "subCategory3": "Chopping Board"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Kitchen Tools",
            "subCategory3": "Mortar & Pestle"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Kitchen Tools",
            "subCategory3": "Moulds"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Kitchen Tools",
            "subCategory3": "Rolling Pins"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Kitchen Tools",
            "subCategory3": "Spatulas"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Kitchen Tools",
            "subCategory3": "Wooden Press"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining",
            "subCategory2": "Terracotta",
            "subCategory3": "Cookware"
        },
        {
            "mainCategory": "HOME & KITCHEN",
            "subCategory1": "Kitchen & Dining, Terracotta",
            "subCategory2": "Drinkware",
            "subCategory3": "Water Bottles"
        },
        {
            "mainCategory": "JEWELLERY",
            "subCategory1": "Womens Jewellery",
            "subCategory2": "Bangles & Bracelets",
            "subCategory3": "Bead Bracelets"
        },
        {
            "mainCategory": "JEWELLERY",
            "subCategory1": "Womens Jewellery",
            "subCategory2": "Bangles & Bracelets",
            "subCategory3": "Oxidised Bracelets"
        },
        {
            "mainCategory": "JEWELLERY",
            "subCategory1": "Womens Jewellery",
            "subCategory2": "Chains & Necklaces",
            "subCategory3": "Chains"
        },
        {
            "mainCategory": "JEWELLERY",
            "subCategory1": "Womens Jewellery",
            "subCategory2": "Chains & Necklaces",
            "subCategory3": "Necklaces"
        },
        {
            "mainCategory": "JEWELLERY",
            "subCategory1": "Womens Jewellery",
            "subCategory2": "Earrings",
            "subCategory3": "Bead Earrings"
        },
        {
            "mainCategory": "JEWELLERY",
            "subCategory1": "Womens Jewellery",
            "subCategory2": "Earrings",
            "subCategory3": "Cubic Zirconia (CZ) Earrings"
        },
        {
            "mainCategory": "JEWELLERY",
            "subCategory1": "Womens Jewellery",
            "subCategory2": "Earrings",
            "subCategory3": "Earrings in Wood"
        },
        {
            "mainCategory": "JEWELLERY",
            "subCategory1": "Womens Jewellery",
            "subCategory2": "Earrings",
            "subCategory3": "Oxidised Earrings"
        },
        {
            "mainCategory": "JEWELLERY",
            "subCategory1": "Womens Jewellery",
            "subCategory2": "Earrings",
            "subCategory3": "Pearl Earrings"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Children's Clothing",
            "subCategory2": "Girls Clothing",
            "subCategory3": "Cardigans"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Children's Clothing",
            "subCategory2": "Girls Clothing",
            "subCategory3": "Frocks"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Children's Clothing",
            "subCategory2": "Girls Clothing",
            "subCategory3": "Langa & Blouse"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Children's Clothing",
            "subCategory2": "Girls Clothing",
            "subCategory3": "Overcoats"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Children's Clothing",
            "subCategory2": "Girls Clothing",
            "subCategory3": "Shirts, Tops & Tunics"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Children's Clothing",
            "subCategory2": "Girls Clothing",
            "subCategory3": "Shrugs"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Clothing & Accessories",
            "subCategory2": "Men's Accessories",
            "subCategory3": "Handkerchiefs"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Clothing & Accessories",
            "subCategory2": "Women's Accessories",
            "subCategory3": "Handkerchiefs"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Girls Clothing",
            "subCategory2": "Girls Clothing",
            "subCategory3": "Skirts & Tops"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Home & Kitchen",
            "subCategory2": "Bath Linen",
            "subCategory3": "Towels"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Home & Kitchen",
            "subCategory2": "Bed Linen",
            "subCategory3": "Bed Spreads"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Home & Kitchen",
            "subCategory2": "Bed Linen",
            "subCategory3": "Bedsheets"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Home & Kitchen",
            "subCategory2": "Bed Linen",
            "subCategory3": "Blankets"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Home & Kitchen",
            "subCategory2": "Home Furnishing",
            "subCategory3": "Appliance covers"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Home & Kitchen",
            "subCategory2": "Home Furnishing",
            "subCategory3": "Carpets"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Home & Kitchen",
            "subCategory2": "Home Furnishing",
            "subCategory3": "Curtains"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Home & Kitchen",
            "subCategory2": "Home Furnishing",
            "subCategory3": "Cushion Covers"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Home & Kitchen",
            "subCategory2": "Home Furnishing",
            "subCategory3": "Floor Runners"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Home & Kitchen",
            "subCategory2": "Table Linen",
            "subCategory3": "Table Cloth"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Infants Clothing",
            "subCategory2": "Girls Clothing",
            "subCategory3": "Frocks"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Infants Clothing",
            "subCategory2": "Girls Clothing",
            "subCategory3": "Skirts & Tops"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Infants Clothing",
            "subCategory2": "Seasonal Wear",
            "subCategory3": "Frocks"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Infants Clothing",
            "subCategory2": "Seasonal Wear",
            "subCategory3": "Skirts"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Men's Clothing",
            "subCategory2": "Ethnic Wear",
            "subCategory3": "Dhotis, Mundus & Lungis"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Men's Clothing",
            "subCategory2": "Fabric",
            "subCategory3": "Khadi Shirt Fabric"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Men's Clothing",
            "subCategory2": "Fabric",
            "subCategory3": "Khadi Trouser Fabric"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Men's Clothing",
            "subCategory2": "Loungewear & Nightwear",
            "subCategory3": "Nightwear"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Men's Clothing",
            "subCategory2": "Loungewear & Nightwear",
            "subCategory3": "Shorts"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Men's Clothing",
            "subCategory2": "Shirts",
            "subCategory3": "Cotton"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Men's Clothing",
            "subCategory2": "Shirts",
            "subCategory3": "Kalamkari"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Men's Clothing",
            "subCategory2": "Shirts",
            "subCategory3": "Khadi"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Sports, Fitness & Outdoors",
            "subCategory2": "Exercise & fitness",
            "subCategory3": "Yoga Mats"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's & Girls Clothing",
            "subCategory2": "Western Wear",
            "subCategory3": "Jackets"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's & Girls Clothing",
            "subCategory2": "Western Wear",
            "subCategory3": "Overcoats"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's & Girls Clothing",
            "subCategory2": "Western Wear",
            "subCategory3": "Ponchos"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's & Girls Clothing",
            "subCategory2": "Western Wear",
            "subCategory3": "Shirts, Tops & Tunics"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's & Girls Clothing",
            "subCategory2": "Western Wear",
            "subCategory3": "Skirts"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's & Girls Clothing",
            "subCategory2": "Western Wear",
            "subCategory3": "Skirts & Tops"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Batik",
            "subCategory3": "Cotton Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Batik",
            "subCategory3": "Georgette Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Batik",
            "subCategory3": "Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Bhagalpuri",
            "subCategory3": "Cotton Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Dharmavaram",
            "subCategory3": "Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ethnic Wear",
            "subCategory3": "Cotton Saree"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ethnic Wear",
            "subCategory3": "Cotton Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ethnic Wear",
            "subCategory3": "Cotton Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ethnic Wear",
            "subCategory3": "Georgette Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ethnic Wear",
            "subCategory3": "Kalamkari Cotton Dupatta"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ethnic Wear",
            "subCategory3": "Kalamkari Silk Dupatta"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ethnic Wear",
            "subCategory3": "Linen Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ethnic Wear",
            "subCategory3": "Salwar Kameez"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ethnic Wear",
            "subCategory3": "Salwar kameez Fabric"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ethnic Wear",
            "subCategory3": "Saree Blouse Fabric"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ethnic Wear",
            "subCategory3": "Saree Patches"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ethnic Wear",
            "subCategory3": "Saree Petticoats"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ethnic Wear",
            "subCategory3": "Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Gadwal",
            "subCategory3": "Cotton Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Gadwal",
            "subCategory3": "Cotton Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Gadwal",
            "subCategory3": "Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ikat",
            "subCategory3": "Saree Blouse Fabric"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Ilkal",
            "subCategory3": "Cotton Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Kalamkari",
            "subCategory3": "Cotton Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Kalamkari",
            "subCategory3": "Cotton Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Kalamkari",
            "subCategory3": "Fabric"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Kalamkari",
            "subCategory3": "Jackets"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Kalamkari",
            "subCategory3": "Salwar kameez Fabric"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Kalamkari",
            "subCategory3": "Saree Blouse Fabric"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Kalamkari",
            "subCategory3": "Silk Dupattas"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Kalamkari",
            "subCategory3": "Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Kanchipuram",
            "subCategory3": "Cotton Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Kanchipuram",
            "subCategory3": "Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Kota",
            "subCategory3": "Cotton Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Kuppadam",
            "subCategory3": "Cotton Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Mangalagiri",
            "subCategory3": "Cotton Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Mangalagiri",
            "subCategory3": "Cotton Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Mangalagiri",
            "subCategory3": "Salwar kameez"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Mangalagiri",
            "subCategory3": "Salwar Kameez Fabric"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Mangalgiri",
            "subCategory3": "Cotton Saree"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Nightwear",
            "subCategory3": "Nighty"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Pochampalli",
            "subCategory3": "Cotton Saree"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Pochampally",
            "subCategory3": "Cotton Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Pochampally",
            "subCategory3": "Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Seasonal Wear",
            "subCategory3": "Overcoats"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Shirts, Tops & Tunics",
            "subCategory3": "Salwar Kameez"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Tie & Dye",
            "subCategory3": "Cotton Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Uppada",
            "subCategory3": "Cotton Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Uppada",
            "subCategory3": "Cotton Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Uppada",
            "subCategory3": "Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Uppada",
            "subCategory3": "Tissue Saree"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Venkatagiri",
            "subCategory3": "Cotton Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Venkatagiri",
            "subCategory3": "Cotton Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Venkatagiri",
            "subCategory3": "Silk Sarees"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Western Wear",
            "subCategory3": "Ponchos"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Western Wear",
            "subCategory3": "Shirts, Tops & Tunics"
        },
        {
            "mainCategory": "TEXTILES",
            "subCategory1": "Women's Clothing",
            "subCategory2": "Western Wear",
            "subCategory3": "Shrugs"
        }
    ];

const hierarchy = {
    subCategories: {}
};

function addToCategory(_hierarchy, category) {

    if (category.id.length <= 0) {
        return _hierarchy;
    }

    _hierarchy.subCategories = _hierarchy.subCategories || {};
    _hierarchy.subCategories[category.id] = _hierarchy.subCategories[category.id] || {
        id: category.id,
        value: category.value
    };

    return _hierarchy;
}


function getNormalizedCategory(data) {
    data = (data || '').trim();
    
    let _id = (data || '').replace(/[,\.;\-\'\(\)]/g, '');
    _id = _id.replace(/[\&]/g, 'AND').replace(/\s/g, '_').toUpperCase()

    return {
        id: _id,
        value: data
    };
}

function constructHierarchy() {

    data.forEach(d => {

        const mainCategory = getNormalizedCategory(d.mainCategory);
        const subCategory1 = getNormalizedCategory(d.subCategory1);
        const subCategory2 = getNormalizedCategory(d.subCategory2);
        const subCategory3 = getNormalizedCategory(d.subCategory3);

        addToCategory(hierarchy, mainCategory);


        addToCategory(hierarchy.subCategories[mainCategory.id], subCategory1);
        addToCategory(hierarchy.subCategories[mainCategory.id].subCategories[subCategory1.id], subCategory2);
        addToCategory(hierarchy.subCategories[mainCategory.id].subCategories[subCategory1.id].subCategories[subCategory2.id], subCategory3);


        // console.log((hierarchy));
    });
}

/**
 * Processes categories in view consumable value
 */
function _processCategories(category) {

    // No subcategories return empty hand
    if (!category.subCategories) {
        return category;
    }

    // List of new categories
    var _categories = [];

    // Map all subcategories keys 
    const _subCategories = Object.keys(category.subCategories);

    for (var i = 0; i < _subCategories.length; i++) {

        const id = _subCategories[i];
        
        var _item = category.subCategories[id];

        _categories.push(_processCategories(_item));
    }

    delete category.subCategories;
    category.subCategories = _categories;
    
    return category;
}

constructHierarchy();
const searchCategory = _processCategories({subCategories: hierarchy.subCategories});

console.log('\n\n\n\n\n\n\n\n\n\n\n\n\n');

console.log(JSON.stringify(hierarchy));
console.log(JSON.stringify(searchCategory));