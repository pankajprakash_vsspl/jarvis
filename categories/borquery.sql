INSERT INTO state_transactions (state, action, nextstate) VALUES 
  ('BOR_REQUEST', 'bor_moveToNegotiate', 'BOR_NEGOTIATE'),
  ('BOR_REQUEST', 'bor_close', 'BOR_CLOSED'),
  ('BOR_NEGOTIATE', 'bor_update', 'BOR_NEGOTIATE'),
  ('BOR_NEGOTIATE', 'bor_createSku', 'BOR_SKU_READY'),
  ('BOR_NEGOTIATE', 'bor_close', 'BOR_CLOSED'),
  ('BOR_SKU_READY', 'bor_skuPublished', 'BOR_SKU_PUBLISHED'),
  ('BOR_SKU_READY', 'bor_close', 'BOR_CLOSED'),
  ('BOR_SKU_PUBLISHED', 'bor_ordered', 'BOR_ORDERED'),
  ('BOR_SKU_PUBLISHED', 'bor_close', 'BOR_CLOSED'),
  ('BOR_ORDERED', null, null),		
  ('BOR_CLOSED', null, null);

INSERT INTO biz_state_transactions (state, action, nextstate) VALUES 
  ('BOR_REQUEST', 'bor_moveToNegotiate', 'BOR_NEGOTIATE'),
  ('BOR_REQUEST', 'bor_close', 'BOR_CLOSED'),
  ('BOR_NEGOTIATE', 'bor_update', 'BOR_NEGOTIATE'),
  ('BOR_NEGOTIATE', 'bor_createSku', 'BOR_SKU_READY'),
  ('BOR_NEGOTIATE', 'bor_close', 'BOR_CLOSED'),
  ('BOR_SKU_READY', 'bor_skuPublished', 'BOR_SKU_PUBLISHED'),
  ('BOR_SKU_READY', 'bor_close', 'BOR_CLOSED'),
  ('BOR_SKU_PUBLISHED', 'bor_ordered', 'BOR_ORDERED'),
  ('BOR_SKU_PUBLISHED', 'bor_close', 'BOR_CLOSED'),
  ('BOR_ORDERED', null, null),		
  ('BOR_CLOSED', null, null);



insert into state_role_action_lookup(state, actions, admin_actions, buyer_actions) values
  ('BOR_REQUEST', '{ "bor_moveToNegotiate": "MOVE TO NEGOTIATE", "bor_close": "CLOSE" }', '{ "bor_moveToNegotiate": "MOVE TO NEGOTIATE", "bor_close": "CLOSE" }', '{"bor_close": "CLOSE" }'),
  ('BOR_NEGOTIATE', '{ "bor_update": "UPDATE", "bor_createSku": "CREATE SKU", "bor_close": "CLOSE" }', '{ "bor_update": "UPDATE", "bor_createSku": "CREATE SKU", "bor_close": "CLOSE" }', '{ "bor_update": "UPDATE", "bor_close": "CLOSE" }'),
  ('BOR_SKU_READY', '{ "bor_skuPublished": "SKU PUBLISHED", "bor_close": "CLOSE" }', '{ "bor_skuPublished": "SKU PUBLISHED", "bor_close": "CLOSE" }', '{ "bor_close": "CLOSE" }'),
  ('BOR_SKU_PUBLISHED', '{ "bor_ordered": "SKU ORDERED", "bor_close": "CLOSE"}', '{ "bor_ordered": "SKU ORDERED", "bor_close": "CLOSE"}', '{ "bor_close": "CLOSE"}'),
  ('BOR_ORDERED', null, null, null),
  ('BOR_CLOSED', null, null, null);

insert into biz_state_role_action_lookup(state, actions, admin_actions, buyer_actions) values
  ('BOR_REQUEST', '{ "bor_moveToNegotiate": "MOVE TO NEGOTIATE", "bor_close": "CLOSE" }', '{ "bor_moveToNegotiate": "MOVE TO NEGOTIATE", "bor_close": "CLOSE" }', '{"bor_close": "CLOSE" }'),
  ('BOR_NEGOTIATE', '{ "bor_update": "UPDATE", "bor_createSku": "CREATE SKU", "bor_close": "CLOSE" }', '{ "bor_update": "UPDATE", "bor_createSku": "CREATE SKU", "bor_close": "CLOSE" }', '{ "bor_update": "UPDATE", "bor_close": "CLOSE" }'),
  ('BOR_SKU_READY', '{ "bor_skuPublished": "SKU PUBLISHED", "bor_close": "CLOSE" }', '{ "bor_skuPublished": "SKU PUBLISHED", "bor_close": "CLOSE" }', '{ "bor_close": "CLOSE" }'),
  ('BOR_SKU_PUBLISHED', '{ "bor_ordered": "SKU ORDERED", "bor_close": "CLOSE"}', '{ "bor_ordered": "SKU ORDERED", "bor_close": "CLOSE"}', '{ "bor_close": "CLOSE"}'),
  ('BOR_ORDERED', null, null, null),
  ('BOR_CLOSED', null, null, null);

-- ('BOR_REQUEST', '{"bor_moveToNegotiate": "MOVE TO NEGOTIATE","bor_close": "CLOSE"}'),
-- ('BOR_NEGOTIATE', '{"bor_update": "UPDATE","bor_createSku": "CREATE SKU","bor_close": "CLOSE"}'),
-- ('BOR_SKU_CREATION_PENDING', '{"bor_skuCreatedApproved": "SKU APPROVED","bor_close": "CLOSE"}'),
-- ('BOR_SKU_READY', '{"bor_skuPublished": "SKU PUBLISHED","bor_close": "CLOSE"}'),
-- ('BOR_CLOSED', null),
-- ('BOR_SKU_PUBLISHED', null),
-- ('BOR_ORDERED', null);